[TOC]

# Installationsguide
- Um die Web-Applikation *eduwallchart* zu starten benötigt es einen Server und eine funktionierende Datenbankverbindung. Es empfiehlt sich eine auf MySQL aufbauende oder SQLite Datenbank. Im Folgenden sind verschiedene Möglichkeiten für Server und Datenbanken erläutert.
- **Es wird ein Java Development Kit (JDK7) vorrausgesetzt.**


## Datenbank einrichten und starten
### MySQL
#### Installation
##### Windows
- MySQL Server installieren (mit vorgegebener Standardkonfiguration vom Installer)
- MySQL Server starten mit: `"...\MySQL Server 5.x\bin\mysqld"`
- MySQL Shell starten mit `"...\MySQL Server 5.x\bin\mysql.exe" "-u root" "-p"`
  - Hierbei muss der richtige Installationspfad angegeben werden.
  - Sollte kein Passwort bei der Installation gesetzt sein, kann die Passwortabfrage mit *ENTER* bestätigt werden.
- Neuen Benutzer anlegen mit: `CREATE USER 'ewchart'@'localhost' IDENTIFIED BY 'ewchart';GRANT ALL PRIVILEGES ON *.* TO 'ewchart'@'localhost' IDENTIFIED BY 'ewchart' WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;`
- Datenbank *ewchart* anlegen: `CREATE DATABASE ewchart;`
- MySQL mit der Eingabe von `exit` beenden.

##### Debian basierte OS
- `sudo apt-get install mysql-server`
- Ein root Passwort für die Datenbank kann erstellt werden, ist aber nicht zwingend notwendig
- MySQL Server starten mit:`sudo mysqld`
- MySQL Shell starten mit: `sudo mysql`
- Neuen Benutzer anlegen in ``MySQL Shell` mit: `CREATE USER 'ewchart'@'localhost' IDENTIFIED BY 'ewchart';GRANT ALL PRIVILEGES ON *.* TO 'ewchart'@'localhost' IDENTIFIED BY 'ewchart' WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;`
- Datenbank *ewchart* anlegen: `CREATE DATABASE ewchart;`
- MySQL Shell mit der Eingabe von `exit` beenden.

#### Server Starten
##### Windows
- `...\MySQL\bin\mysqld.exe` in Eingabeaufforderung ausführen

##### Debian basierte OS
- `sudo mysqld`

### MariaDB
#### Installation
##### Windows
- MariaDB herunterladen und entpacken
- Da MariaDB auf MySQL aufbaut, können die gleichen Schritte wie oben für MySQL beschrieben ausgeführt werden

##### Debian basierte OS
- MariaDB Server installieren `sudo apt-get install mariadb-server`
- MySQL Server starten mit:`sudo mysqld`
- MySQL Shell starten mit: `sudo mysql`
- Neuen Benutzer anlegen in ``MySQL Shell` mit: `CREATE USER 'ewchart'@'localhost' IDENTIFIED BY 'ewchart';GRANT ALL PRIVILEGES ON *.* TO 'ewchart'@'localhost' IDENTIFIED BY 'ewchart' WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;`
- Datenbank *ewchart* anlegen: `CREATE DATABASE ewchart;`
- MySQL Shell mit der Eingabe von `exit` beenden

#### Server starten
##### Windows
- `...\MariaDB\bin\mysqld.exe` in Eingabeaufforderung ausführen

##### Debian basierte OS
- `sudo mysqld`

### SQLite
#### Windows
- SQLite herzunterladen und entpacken
- SQLite Shell ausführen
- `.save database.db` erzeugt eine neue SQLite Datenbank mit dem Namen *database,db* im aktuellen Verzeichnis
- *database.db* in beliebigen Ordner verschieben
- Absoluten Pfad zur Datenbank in *database.properties* angeben. Dies könnte wie folgt aussehen:
```java
javax.persistence.jdbc.url=jdbc:sqlite:C:/Dokumente und Einstellungen/benutzer/eduwallchart/database.db
javax.persistence.jdbc.driver=org.sqlite.JDBC
```

#### Debian basierte OS
- `sudo apt-get install sqlite3`
- `sqlite3 database.db` ausführen
- `.exit` beendet die sqlite Shell und speichert die Datei *database.db* im Home Directory
- *database.db* in beliebigen Ordner verschieben

## Server einrichten und starten
### Tomcat
#### Windows
- Tomcat 7.0 herunterladen und entpacken
- jdk installieren
- Umgebungsvariablen setzen
 - Start -> Rechtsklick auf *Arbeitsplatz* -> Eigenschaften -> Erweitert -> Umgebungsvariablen
 - dort sofern nicht vorhanden für den Benutzer folgende Umgebungsvariablen hinzufügen (Der Pfad muss je nach Installationsort angepasst werden)
   - Name der Variablen: **JAVA_HOME**
   - Wert der Variablen: **C:\Programme\Java\jdk1.7.0_60**
   - Name der Variablen: **CATALINA_HOME**
   - Wert der Variablen: **C:\Programme\apache-tomcat-7.0.54**
   - mit OK bestätigen und alle offenen Eingabeaufforderungen schließen
- Webarchiv *eduwallchart.war* in Ordner `...\apache-tomcat-7.x.x\webapps\` kopieren
- Datenbank einrichten und starten **bevor** der Server gestartet wird (siehe oben)
- Datenbankverbindung in *database.properties* Datei schreiben (siehe *Datenbank auswählen* im Handbuch)
- `start C:\Programme\apache-tomcat-7.0.54\bin\startup.bat` ausführen, um Server zu starten

#### Debian basierte OS
- Tomcat 7 herunterladen und entpacken
- Webarchiv in *webapps* Ordner von Tomcat entpacken
- Datenbank einrichten und starten **bevor** der Server gestartet wird (siehe oben)
- Alle `*.sh` und `.jar` Dateien ausführbar machen mit `chmod +x datei` Zum Beispiel:
 - `chmod +x startup.sh`
 - `chmod +x shutdown.sh`
 - `chmod +x catalina.sh`
 - `chmod +x setclasspath.sh`
 - `chmod +x bootstrap.jar`
- Datenbankverbindung in *database.properties* Datei schreiben (siehe *Datenbank auswählen*)
- Server starten mit `tomcat7/bin/startup.sh`

### Jetty
#### Windows
- Jetty 8.1 herunterladen und entpacken
- Webarchiv *eduwallchart.war* in Ordner `...\jetty-distribution-8.1.x.x\webapps\` kopieren
- Datenbank einrichten und starten **bevor** der Server gestartet wird (siehe oben)
- Datenbankverbindung in *database.properties* Datei schreiben (siehe *Datenbank auswählen*)
- In Eingabeaufforderung mit `cd c:\path\to\jetty\` in das Verzeichnis wechseln, wo jetty entpackt wurde
- Jetty starten mit `start java -jar start.jar`

#### Debian basierte OS
- Jetty 8.1 herunterladen und entpacken
- Webarchiv *eduwallchart.war* in Ordner `...\jetty-distribution-8.1.x.x\webapps\` kopieren
- Datenbank einrichten und starten **bevor** der Server gestartet wird (siehe oben)
- Datenbankverbindung in *database.properties* Datei schreiben (siehe *Datenbank auswählen*)
- In Konsole mit `/path/to/jetty/` in das Verzeichnis wechseln, wo jetty entpackt wurde
- Jetty starten mit `start java -jar start.jar`

