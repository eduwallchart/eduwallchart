# Handbuch für eduWallChart

[TOC]

## Einleitung

Die Webanwendung eduWallChart stellt einen wichtigen Bestandteil der Lehrdruckerei der Fakultät Medien der HTWK Leipzig dar.
Die Anwendung visualisiert die Abläufe der Druckerei.
Dies geschieht in Form einer Plantafel, bei welcher alle Planungs- und Ausführunghorizonte aufgeführt und dargestellt werden.
Die Plantafel beinhaltet somit beispielsweise Informationen über die vorhandenen Maschinen, die Aufträge, aber auch über Unregelmäßigkeiten oder Störungen.

Die Software eduWallChart teilt sich hierbei in zwei Bestandteile auf: die Plantafel, welche vom Anwender verwendet wird, und die Schnittstelle zum Prinect-System, welche serverseitig realisiert wird.
Das ganze System ist dabei passiv gestaltet: es werden lediglich Informationen dargestellt, welche über Prinect verwaltet und bearbeitet werden.
Die Kommunikation zwischen eduWallChart und Prinect ist demnach einseitig – eduWallChart erhält die Daten von Prinect, eine Kommunikation in die andere Richtung erfolgt aber nicht.

Die Plantafel selber bietet eine eine kompakte Darstellung der Vorgänge. Zu den einzelnen Maschinen oder Aufträgen sind außerdem noch detailliertere Informationen einsehbar.
Diese werden beim Klicken oder Hovern der entsprechenden Maschine beziehungsweise des entsprechenden Auftrags angezeigt.

Der von der Plantafel abgedeckte Zeitraum ist des Weiteren einstellbar. So kann er von einem einzelnen Tag bis hin zu einem ganzen Semester betragen.

Im Rahmen dieses Handbuchs kann beispielsweise nachgelesen werden, welche Möglichkeiten der Konfigurierung bestehen und wie diese angewendet werden.
Außerdem wird ausführlich die Installation der Anwendung auf dem Server aufgearbeitet.

## Module

### Einlesen von JDF-Dateien

Um neue bzw. Aktualisierte JDF-Dateien einzulesen, müssen diese im eingestellten Hotfolder abgelegt werden.

Wenn das JDF die richtige Form hat, werden die Daten importiert und daraufhin in den Ordner ~/jdf/success verschoben.
Falls es zu Fehlern beim Parsen kommt, wird der gesamte Inhalt ignoriert und in das Verzeichniss ~/jdf/failed verschoben.
In beiden Fällen wird ein Log über die Verarbeitung des Files an der selben Stelle wie das jdf abgelegt.
Der Dateiname des Logs ist gleich dem des Archiv-JDFs (Zeitstempel+Orginalname), mit dem Suffix '.log'.

### Aktualisierungsbutton in der Oberfläche

- Es ist möglich, neu eingelesene Daten (Jobs oder Maschinen) anzeigen zu lassen, ohne die gesamte Seite im Browser neu zu laden.
- Hierzu muss nun lediglich auf den Button "Aktualisieren" links oberhalb der Maschinen gedrückt werden.
- Sollten die angezeigten Daten bereits aktuell sein, bleiben diese bestehen.

### Ordnerstruktur

- Die Ordnerstruktur ist wie folgt festgelegt:
 - home directory
   - eduwallchart
     - hotfolder
     - jdf
        - success
        - failed
     - logs
     - templates
        - jobs
        - machines
     - impressum
     - *config*
     - *Assets*
- *kursive* Einträge sind noch nicht vorhanden und werden so oder ähnlich noch umgesetzt.

### Impressum

Um der Anwendung ein Impressum hinzuzufügen muss eine Datei ```impressum.html``` in den Ordner ```impressum``` geladen werden.
In der Datei können HTML-Tags benutzt werden, jedoch muss kein HTML-Schema aufgebaut werden (```<html>```, ```<head>```, etc.).
In der Datei steht demnach das, was im ```<body>``` der HTML-Datei stehen würde.
Wenn keine Datei hochgeladen wurde, kann auch kein Impressum angezeigt werden.
Beim Klick auf "Impressum" in der Oberfäche erscheint dann nur die Meldung "Beim Lesen des Impressums ist ein Fehler aufgetreten.".

## Verwendung von eduWallChart

### Detail-Informationen

Zu den Maschinen und den Aufträgen der Plantafel können noch weitere Details angezeigt werden.
Diese werden nach einem Klick auf die gewünschte Maschine, beziehungsweise den gewünschten Auftrag, geöffnet.
Die Darstellung der Detail-Informationen kann außerdem auch noch über XSLT angepasst werden.
Hierfür können per FTP-Zugang verschiedene Templates für die Maschinen und die Aufträge hochgeladen werden.
Die vorhandenen Templates können in den jeweiligen Select-Feldern bei den Optionen ausgewählt werden.
Die Darstellung erfolgt dann bis auf weiteres mit dem gewählten Templates (die Auswahl wird als Cookie gespeichert).

Um ein Template auszuwählen, muss das gewünschte Template im Menü (Zahnrad oben rechts) angewählt werden.
Die Auswahl muss anschließend durch einen Klick auf den Button "Speichern" bestätigt werden.
Bereits offene Tooltips behalten ihren alten Text.
Das neue ausgewählte Template wird demnach erst nach dem Schließen und dem erneuten Öffnen eines Tooltips berücksichtigt.

#### XSLT-Informationen und Beispiel-XSLT

Die Detail-Informationen können mittels XSLT beeinflusst werden.
Standardmäßig werden alle verfügbaren Informationen der Maschinen und der Jobs angezeigt.
Die XSLT-Dateien müssen per FTP in den Ordner `templates/machines` beziehungsweise `templates/jobs` geladen werden.
Diese werden dann verarbeitet (auch bei Server-Start) und - sofern sie valide sind - im Menü in der Oberfläche zur Auswahl angeboten.
Sollte eine XSLT-Datei nicht valide sein, so wird dies in der Log-Datei vermerkt.
Wichtig ist außerdem auch, dass das Template einen Namen besitzt (zum Beispiel `<xsl:template name="MeinTemplate" match="machine">`).
Dieser wird in der Oberfläche angezeigt.
Des Weiteren darf der Name keine Leerzeichen besitzen.
Bei den XSLTs können auf folgende Informationen zugegriffen werden:

- `machine`
    - `id`
    - `name`
    - `machineStatus`
- `job`
    - `id`
    - `name`
    - `startTime`
    - `endTime`

Eine XSLT-Datei könnte beispielsweise wie folgt aufgebaut sein:

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template name="Machine-Default" match="machine">
        <xsl:if test="id != ''">
            <div class="property">
                <div class="name">ID:</div>
                <div class="value">
                    <xsl:value-of select="id"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="name != ''">
            <div class="property">
                <div class="name">Name:</div>
                <div class="value">
                    <xsl:value-of select="name"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="machineStatus != ''">
            <div class="property">
                <div class="name">Status:</div>
                <div class="value">
                    <xsl:value-of select="machineStatus"/>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
```

### Anzeigezeitraum und Auflösung einstellen

In der Eduwallchart kann man mithilfe zweier Datumsfelder den Anzeigezeitraum benutzerdefiniert anpassen. Die Daten sind frei wählbar solange das Enddatum nicht vor dem Startdatum liegt. Desweiteren kann man zur bequemeren Anzeige eine Auflösung auswählen, welche dann automatisch den Anzeigezeitraum anpasst.

- mithilfe der Buttons "Tag", "Woche" und "Monat" kann der Anzeigezeitraum eingestellt werden
- es kann immer nur *eine* Auflösung gleichzeitig aktiv sein, oder ein benutzerdefinierter Zeitraum
- je nach eingestellter Auflösung, passt sich beim Ändern des Startdatums auch entsprechend das Enddatum an
 - z.B. bei Auflösung *Woche* und dem  Startdatum 1.3.2014, wird automatisch das Enddatum auf 8.3.2014 gesetzt
- wird bei einer *eingestellten* Auflösung das Enddatum manuell verändert, so wird die Auflösung deaktiviert, und der benutzerdefinierte Zeitraum angezeigt
- die Einstellungen werden in Cookies gespeichert, und beim erneuten Aufrufen der Seite auf den letzten Stand (falls vorhanden) zurückgesetzt

### Darstellung der Jobs auf der Timeline

Die Jobs werden nach dem eingestellten Zeitraum gefiltert und entsprechend auf der Timeline angeordnet und skaliert.
Ist der Text innerhalb des Jobs länger als der verfügbare Platz, wird dieser durch "..." abgekürzt.
Der vollständige Text ist dann mithilfe des Tooltips per Klick abrufbar.

Innerhalb der Job-Kachel werden Name, ID und Status des Jobs angezeigt.
Im Hintergrund findet sich außerdem die Fortschrittsanzeige des jeweiligen Jobs.
Diese ist im Standard-Template auch in dem Tooltip sichtbar.
Der angegebene Prozentwert ergibt sich durch Start-, End- und aktuelle Zeit.

#### Marker für die Anzeige des aktuellen Zeitpunktes

Das aktuelle Datum inklusive Uhrzeit und Wochentag wird auf der Timeline durch einen vertikalen schwarzen Balken dargestellt,
der sich je nach gewähltem Anzeigezeitraum der Timeline und in Abhängigkeit zum Enddatum automatisch verschiebt.
Befindet der aktuelle Zeitpunkt außerhalb des gewählten Anzeigezeitraumes, so wird der Marker nicht angezeigt.



### Datenbank auswählen

Um die Datenbank, in der die Maschinen und Jobs gespeichert werden, ändern zu können, müssen die gewünschten Verbindungsdaten in  die Datei *database.properties* (im Homeverzeichnis) geschrieben werden.
Hierbei muss erst der Name der Variablen und danach mit einem *=* verknüpft ihr Wert stehen.
Für den Benutzernamen würde sich z.B. folgender Eintrag ergeben: `javax.persistence.user=ewchart`, wobei `javax.persistence.jdbc.user` für die Variable steht und `ewchart` ihren Wert verkörpert.
Eine Beispieldatei für MySQL könnte wie folgt aussehen:

```java
javax.persistence.jdbc.user=ewchart
javax.persistence.jdbc.password=ewchart
javax.persistence.jdbc.url=jdbc:mysql://localhost:3306/ewchart
javax.persistence.jdbc.driver=com.mysql.jdbc.Driver
```

Um eine SQLite Datenbank zu verwenden, sind lediglich Angaben zum Pfad der Datenbankdatei und der benötigte Treiber notwendig, Benutzername und Passwort sind optional.
Eine Beispieldatei könnte wie folgt aussehen:
```java
javax.persistence.jdbc.url=jdbc:sqlite:/home/benutzer/eduwallchart/mysql3.db
javax.persistence.jdbc.driver=org.sqlite.JDBC
```
Eine h2 Datenbank benötigt folgende Einstellungen:
```java 
javax.persistence.jdbc.url=jdbc:h2:tcp://localhost/~/ewchart
javax.persistence.jdbc.driver=org.h2.Driver
```

Die Treiber für MySQL und SQLite sind bereits im Web-Archiv enthalten, um andere Treiber zu benutzen, können diese in den Ordner `WEB-INF/lib` in der .war Datei des Projekts kopiert werden.

Sollte *eduwallchart* keine Verbindung mit der Datenbank aufbauen können, erfolgt eine entsprechende Fehlermeldung im eduwallchart-log und im server-log und der Server stoppt das deployen.
