function calcProgress(job) {
    var now = new Date().getTime();
    var startdate = new Date(job.find('a.startdate .v-label').html()).getTime();
    var enddate = new Date(job.find('a.enddate .v-label').html()).getTime();
    var progress;
    if (now > enddate) {
        progress = 100;
    } else if (now < startdate) {
        progress = 0;
    } else {
        progress = Math.round((now - startdate) * 100 / (enddate - startdate));
    }
    return progress;
}

function updateJobProgress() {
    function setTime() {
        var progress = calcProgress($(this));
        $(this).children('.progress').text(progress + '%');
        $(this).find('.tooltip .v-label .progress-property .value').html(progress + '%');
    }

    $('.job').each(setTime);
}
setInterval(updateJobProgress, 600000);
