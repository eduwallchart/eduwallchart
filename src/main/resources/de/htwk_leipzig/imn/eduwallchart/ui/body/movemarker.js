function movemarker() {
    var startdatevalue = $('.startdate-field .v-datefield-textfield').val().split('.');
    var enddatevalue = $('.enddate-field .v-datefield-textfield').val().split('.');

    var startdate = new Date(startdatevalue[2], startdatevalue[1] - 1, startdatevalue[0], 0, 0, 0);
    var enddate = new Date(enddatevalue[2], enddatevalue[1] - 1, enddatevalue[0], 23, 59, 59);
    var now = new Date();

    if (now > startdate && now < enddate) {
        var distance = (now - startdate) * 100 / (enddate - startdate);
        $('.marker').attr("style", "left: " + distance + "%");
        $('.marker-date').text(createDateString(now));
        $('.marker-time').text(createTimeString(now));
    }
}

function createDateString(date) {
    var datestring = "";
    switch (date.getDay()) {
        case 0:
            datestring = "So, ";
            break;
        case 1:
            datestring = "Mo, ";
            break;
        case 2:
            datestring = "Di, ";
            break;
        case 3:
            datestring = "Mi, ";
            break;
        case 4:
            datestring = "Do, ";
            break;
        case 5:
            datestring = "Fr, ";
            break;
        case 6:
            datestring = "Sa, ";
            break;
    }
    datestring += date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
    return datestring;
}

function createTimeString(date) {
    var time = new String();

    if (date.getHours() < 10) time = "0"
    time += date.getHours();
    if (date.getMinutes() < 10) time += ":0" + date.getMinutes();
    else time += ":" + date.getMinutes();
    if (date.getSeconds() < 10) time += ":0" + date.getSeconds();
    else time += ":" + date.getSeconds();

    return time;
}

setInterval(movemarker, 1000);