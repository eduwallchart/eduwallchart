// Every element with the class .has-tooltip gets an eventHandler when clicked
// eventhandler builds qtip and updates progress if object is a job
$(document).on('click', '.has-tooltip', function () {
    var object = $(this);
    if (object[0].classList.contains("job")) {
        object.find('.tooltip .v-label .progress-property .value').html(calcProgress(object) + '%');
        object.children('.progress').text(calcProgress(object) + '%');
    }
    enableQtip(object, object.find('.tooltip').clone());
});

$(document).on('click', '.has-dialog', function () {
    var object = $(this);
    enableQtip(object, object.find('.dialog'), false);
});

function enableQtip(object, text, reload) {
    if (reload) {
        object.qtip('destroy');
    }
    object.qtip({
        // Make tooltip not-final, so progress updates from .tooltip can be fetched
        overwrite: true,
        content: {
            // Take the div.tooltip from inside the object as tooltip
            text: text,
            // Set the tooltip text for the close button
            button: 'Schließen'
        },
        show: {
            // when the object is clicked for the first time, it gets the eventHandler,
            // so we have to show the tooltip immediately
            ready: true,
            // show tooltip when object element is clicked
            event: 'click'
        },
        hide: {
            // hide tooltip when object element is clicked
            event: 'click'
        },
        position: {
            // position tooltip to the bottom center of the clicked object
            my: 'top center',
            at: 'bottom center',
            target: object
        }
    });
}
