<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html"/>
    <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz-'"/>
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ_'"/>
    <xsl:template name="Machine-Default" match="machine">
        <xsl:if test="id != ''">
            <div class="property">
                <div class="name">ID:</div>
                <div class="value">
                    <xsl:value-of select="id"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="name != ''">
            <div class="property">
                <div class="name">Name:</div>
                <div class="value">
                    <xsl:value-of select="name"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="machineStatus != ''">
            <div class="property">
                <div class="name">Status:</div>
                <div style="text-align: center; padding: 0.3em;"
                     class="value {translate(machineStatus, $uppercase, $smallcase)}">
                    <xsl:value-of select="translate(machineStatus, $uppercase, $smallcase)"/>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    <xsl:template name="Job-Default" match="job">
        <xsl:if test="id != ''">
            <div class="property">
                <div class="name">ID:</div>
                <div class="value">
                    <xsl:value-of select="id"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="name != ''">
            <div class="property">
                <div class="name">Name:</div>
                <div class="value">
                    <xsl:value-of select="name"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="xmlStartTime != ''">
        <div class="property">
                <div class="name">Start:</div>
                <div class="value">
                    <xsl:value-of select="xmlStartTime"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="xmlEndTime != ''">
            <div class="property">
                <div class="name">Ende:</div>
                <div class="value">
                    <xsl:value-of select="xmlEndTime"/>
                </div>
            </div>
        </xsl:if>
        <div class="property progress-property">
            <div class="name">Fortschritt:</div>
            <div class="value"/>
        </div>
        <xsl:if test="status != ''">
            <div class="property">
                <div class="name">Status:</div>
                <div style="text-align: center; padding: 0.3em;"
                     class="value {translate(status, $uppercase, $smallcase)}">
                    <xsl:value-of select="translate(status, $uppercase, $smallcase)"/>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>