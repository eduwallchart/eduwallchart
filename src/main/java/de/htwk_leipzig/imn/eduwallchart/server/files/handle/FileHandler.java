package de.htwk_leipzig.imn.eduwallchart.server.files.handle;

import java.nio.file.Path;
import java.nio.file.WatchEvent;


/**
 * This Interface handles a Path from one class to another.
 *
 * @author Peter Nancke
 *         review/test: Georg Felbinger
 */
public interface FileHandler {

    /**
     * @param pathToFile gives the class a Path
     */
    void handleFile(Path pathToFile, WatchEvent.Kind kind) throws HandleFileException;

}
