package de.htwk_leipzig.imn.eduwallchart.server.jdf;

/**
 * Exception, die geworfen wird, wenn ein Link zu einer Maschine angelegt werden soll, die nicht vorhanden ist
 *
 * @author Georg Felbinger
 */
class MachineNotFoundException extends JdfParsingException {

    MachineNotFoundException(String description) {
        super(description);
    }
    MachineNotFoundException(String description, Exception cause) {
        super(description, cause);
    }
    MachineNotFoundException(Exception cause) {
        super(cause);
    }
}
