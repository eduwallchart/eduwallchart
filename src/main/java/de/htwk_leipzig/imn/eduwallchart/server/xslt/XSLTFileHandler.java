package de.htwk_leipzig.imn.eduwallchart.server.xslt;

import de.htwk_leipzig.imn.eduwallchart.server.files.handle.FileHandler;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.HandleFileException;
import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;

/**
 * XSLTFileHandler behandelt die XSLT-Dateien, die in die jeweiligen Ordner eingefügt werden.
 * Diese werden dann in einem XSLTManager verarbeitet.
 *
 * @author Florian Klos
 *         Reviewer: Georg Felbinger
 */
public class XSLTFileHandler implements FileHandler {

    private final Logger logger;
    private final XSLTManager xsltManager;

    public XSLTFileHandler(Logger logger, XSLTManager xsltManager) {
        this.logger = logger;
        this.xsltManager = xsltManager;
    }

    /**
     * Methode zum Behandeln von XSLTs
     *
     * @param pathToFile Pfad zum XSLT
     * @param kind       Art der Änderung
     * @throws HandleFileException
     */
    @Override
    public void handleFile(Path pathToFile, WatchEvent.Kind kind) throws HandleFileException {
        logger.info("Handling Machine-XSLT '" + pathToFile.toString() + "'");
        if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
            xsltManager.removeXSLT(pathToFile);
        } else {
            try {
                XSLT xslt = createXSLT(pathToFile);
                xsltManager.createOrUpdateXSLT(xslt);
            } catch (ParserConfigurationException | IOException | SAXException | DuplicateNameException e) {
                logger.error("Template '" + pathToFile.toString() + "' ist nicht valide und ist daher nicht verfügbar.");
                throw new HandleFileException(e);
            }
        }
    }

    private XSLT createXSLT(Path pathToFile) throws ParserConfigurationException, IOException, SAXException {
        File xsltFile = pathToFile.toFile();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xsltFile);
        NodeList nodes = doc.getElementsByTagName("xsl:template");
        if (nodes.getLength() != 1 ||
                nodes.item(0).getAttributes().getNamedItem("name") == null) {
            throw new ParserConfigurationException();
        }
        String name = nodes.item(0).getAttributes().getNamedItem("name").getNodeValue();
        return new XSLT(name, pathToFile);
    }
}
