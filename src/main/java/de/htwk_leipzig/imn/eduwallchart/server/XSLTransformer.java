package de.htwk_leipzig.imn.eduwallchart.server;

import com.google.common.io.Files;
import de.htwk_leipzig.imn.eduwallchart.persistence.Job;
import de.htwk_leipzig.imn.eduwallchart.persistence.Machine;

import javax.xml.bind.JAXB;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Path;

/**
 * Die Klasse XSLTransformer bietet Methoden, welche die Maschinen- und die Job-Objekte als HTML darstellen.
 * Die Transformation berücksichtigt dabei das gewählte XSLT-Template.
 *
 * @author Florian Klos
 * Reviewer: Georg Felbinger
 */
public class XSLTransformer {

    //@Inject //logger //logger;

    /**
     * getMachineTooltip liefert den HTML-Code des anzuzeigenden Tooltips.
     * Der Code wird abhängig vom XSLT-Template aufgebaut.
     *
     * @param machine das Maschinenobjekt
     * @param xslt Pfad zur verwendeten XSLT
     * @return HTML-Code als String
     */
    public String getMachineTooltip(Machine machine, Path xslt) {
        try {
            Reader machineXSL = Files.newReader(xslt.toFile(), Charset.forName("UTF-8"));
            return getTransformedOutput(getObjectAsXML(machine), machineXSL);
        } catch (Exception e) {
            //logger.error(e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    /**
     * getJobTooltip liefert den HTML-Code des anzuzeigenden Tooltips.
     * Der Code wird abhängig vom XSLT-Template aufgebaut.
     *
     * @param job das Jobobjekt
     * @param xslt Pfad zur verwendeten XSLT
     * @return HTML-Code als String
     */
    public String getJobTooltip(Job job, Path xslt) {
        try {
            Reader jobXSL = Files.newReader(xslt.toFile(), Charset.forName("UTF-8"));
            return getTransformedOutput(getObjectAsXML(job), jobXSL);
        } catch (Exception e) {
            e.printStackTrace();
            //logger.error(e.getMessage());
            return "";
        }
    }

    private String getObjectAsXML(Object object) {
        Writer writer = new StringWriter();
        JAXB.marshal(object, writer);
        return writer.toString();
    }

    private String getTransformedOutput(String xml, Reader xslt) {
        Source xmlInput = new StreamSource(new StringReader(xml));
        Source xsltInput = new StreamSource(xslt);
        Writer writer = new StringWriter();
        Result result = new StreamResult(writer);
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer(xsltInput);
            transformer.transform(xmlInput, result);
            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return xml;
        }
    }
}
