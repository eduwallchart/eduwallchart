package de.htwk_leipzig.imn.eduwallchart.server.jdf;


import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;

/**
 * @author Georg Felbinger, 03.06.14.
 */
abstract class JdfLoggerFactory {

    public static Logger getLogger(Path logFile) {
        Logger logger = (Logger) LoggerFactory.getLogger("JdfLogger");
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();

        logger.addAppender(createFileAppender(logFile, loggerContext, createPatternLayout(loggerContext)));
        logger.setAdditive(false);
        logger.setLevel(Level.ALL);

        return logger;
    }

    private static PatternLayout createPatternLayout(LoggerContext loggerContext) {
        PatternLayout patternLayout = new PatternLayout();
        patternLayout.setPattern("%d %5p %t [%c:%L] %m%n)");
        patternLayout.setContext(loggerContext);
        patternLayout.start();

        return patternLayout;
    }

    private static FileAppender<ILoggingEvent> createFileAppender(Path logFile, LoggerContext loggerContext, PatternLayout patternLayout) {
        FileAppender<ILoggingEvent> fileAppender = new FileAppender<>();
        fileAppender.setFile(logFile.toString());
        fileAppender.setContext(loggerContext);
        fileAppender.setLayout(patternLayout);
        fileAppender.start();

        return fileAppender;
    }

    public static void destroyLogger(Logger logger) {
        logger.detachAndStopAllAppenders();
    }
}
