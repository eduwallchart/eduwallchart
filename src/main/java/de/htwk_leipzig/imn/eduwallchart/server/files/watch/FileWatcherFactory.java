package de.htwk_leipzig.imn.eduwallchart.server.files.watch;

import de.htwk_leipzig.imn.eduwallchart.server.files.handle.FileHandler;
import org.slf4j.Logger;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

/**
 * Factory Klasse für den FileWatcher.
 * Erstellt neue FileWatcher, der für neu erstellte Dateien im Hotfolder überwacht.
 *
 * @author Peter Nancke
 * Review Willi Reiche am 26.05.2014
 */
public class FileWatcherFactory {


    private FileWatcherFactory() {
    }

    /**
     * Erstellt einen neuen FileWatcher zur Überwachung eines Ordners
     * @param executorService ExecutorService, indem der FileWatcher als Thread gestartet wird.
     * @param fileHandler FileHandler, an den der Pfad der erstellten Datei übergeben wird.
     * @param path Pfad zum JDF Hotfolder
     * @param logger Logger, in dem bestimmte Ereignisse geloggt werden.
     * @param eventList Liste von Events, die der FileWatcher überwachen soll.
     * @return Erstellter FileWatcher
     */
    public static FileWatcher createFileWatcher(ExecutorService executorService, FileHandler fileHandler, Path path, Logger logger, ArrayList<WatchEvent.Kind> eventList) {
        FileWatcher fileWatcher = new FileWatcher(executorService, fileHandler, path, logger);
        fileWatcher.setEvents(eventList);
        return fileWatcher;
    }



}
