package de.htwk_leipzig.imn.eduwallchart.server.jdf;

import com.google.inject.Inject;
import de.htwk_leipzig.imn.eduwallchart.PathModule;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.FileHandler;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.HandleFileException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchEvent;


/**
 * Klasse zum Behandeln von JDF-Files
 *
 * @author Georg Felbinger, 22.01.14
 *         Review: Florian Klos (19.04.2014)
 *         The JdfFileHandler handles JDF-Files.
 */
public class JdfFileHandler implements FileHandler {

    @Inject Logger logger;
    @Inject JdfFileParser jdfFileParser;
    @Inject @PathModule.JdfSuccessPath Path jdfSuccessPath;
    @Inject @PathModule.JdfFailPath Path jdfFailPath;

    /**
     * Implementierte Methode des FileHandler-Interfaces
     * Über diese Methode gibt der FileWatcher gefundene JDFs an den JdfFileHandler weiter
     * Die Methode ruft parsJdfFile() auf, um mit der Verarbeitung der Datei zu beginnen
     *
     * @param pathToFile Pfad zur JDF-Datei, die verarbeitet werden soll.
     */
    @Override
    public void handleFile(Path pathToFile, WatchEvent.Kind kind) throws HandleFileException {
        logger.info("Handling JDF-File: '" + pathToFile.toString() + "'");
        String jdfLogFileName = DateTime.now().toString() + "_" + pathToFile.toFile().getName() + ".log";
        jdfLogFileName = jdfLogFileName.replace(":", "-").replace("+","_");
        Path jdfLogFile = jdfSuccessPath.resolve(jdfLogFileName);
        Logger jdfLogger = (Logger) JdfLoggerFactory.getLogger(jdfLogFile);
        try {
            jdfFileParser.parseJdfFile(pathToFile, jdfLogger);
            logger.info("JDF-File '" + pathToFile.toString() + "' successfully handled. See '" + jdfSuccessPath.resolve(jdfLogFileName).toString() + "' for details.");
        } catch (JdfParsingException e) {
            logger.error("Error handling JDF-File '" + pathToFile.toString() + "'.");
            jdfLogger.error(e.getMessage());
            JdfLoggerFactory.destroyLogger((ch.qos.logback.classic.Logger) jdfLogger);
            moveLogFile(jdfLogFile, jdfFailPath.resolve(jdfLogFileName));
            throw new HandleFileException("Error while parsing JDF. See '" + jdfFailPath.resolve(jdfLogFileName).toString() + "' for details.");
        }
    }

    private void moveLogFile(Path from, Path to) {
        try {
            Files.move(from, to);
        } catch (IOException e) {
            logger.error("Unable to move JDF-Log.", e);
        }
    }
}
