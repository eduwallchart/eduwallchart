package de.htwk_leipzig.imn.eduwallchart.server.files.handle;

/**
 * Exception, die während des Bearbeitens von Dateien geworfen werden kann
 *
 * @author Georg Felbinger
 */
public class HandleFileException extends Exception {
    public HandleFileException(String desc) {
        super(desc);
    }

    public HandleFileException(String desc, Exception cause) {
        super(desc, cause);
    }

    public HandleFileException(Exception cause) {
        super(cause);
    }
}
