package de.htwk_leipzig.imn.eduwallchart.server.files.watch;

import de.htwk_leipzig.imn.eduwallchart.server.files.handle.FileHandler;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.HandleFileException;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static java.util.concurrent.Executors.newSingleThreadExecutor;

/**
 * Startet einen WatchService, um Änderungen am File-System erkennen zu lassen.
 *
 * @author: Peter Nancke
 */
public class FileWatcher {

    private final ExecutorService handlerExecutor;
    private final Logger logger;
    ExecutorService watcherExecutor;
    FileHandler fileHandler;
    Path dir;
    ArrayList<WatchEvent.Kind> events = new ArrayList<>();

    public FileWatcher(ExecutorService handlerExecutor, FileHandler fileHandler, Path dir, Logger logger) {
        this.handlerExecutor = handlerExecutor;
        this.watcherExecutor = newSingleThreadExecutor(); // Executors.newSingleThreadExecutor()
        this.fileHandler = fileHandler;
        this.dir = dir; // configManager.getConfig().getJdfPath();
        this.logger = logger;
    }

    /**
     * Startet den FileWatcher in einem eigenen Thread.
     */
    public void start() {

        Runnable runWatcher;
        runWatcher = new Runnable() {
            @Override
            public void run() {
                try {
                    startWatcher();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        watcherExecutor.execute(runWatcher);
    }

    /**
     * Erstellt einen neuen WatchService, der je nach durch addEvent(WatchEvent.Kind)
     * hinzugefügten Ereignissen, diese(n) erkennt.
     * Wenn der Thread unterbrochen wird, fährt der ExecutorService herunter.
     */
    private void startWatcher() throws IOException, InterruptedException {

        WatchService watcher = FileSystems.getDefault().newWatchService();

        WatchKey key;

        WatchEvent.Kind[] eventArray = events.toArray(new WatchEvent.Kind[events.size()]);
        key = dir.register(watcher, eventArray);

        while (!Thread.currentThread().isInterrupted()) {
            key = waitForEvents(watcher, key);
        }

        watcherExecutor.shutdown();
    }

    /**
     * Wartet mit watcher.take() blockierend auf System-Events und gibt den Pfad des Events an den FileHandler weiter.
     *
     * @param watcher WatchService um System auf Änderungen zu überprüfen
     * @param key     WatchKey enthält das Event
     * @return WatchKey
     */
    private WatchKey waitForEvents(WatchService watcher, WatchKey key) {

        try {
            key = watcher.take();
        } catch (InterruptedException | ClosedWatchServiceException e) {
            Thread.currentThread().interrupt();
        }

        for (WatchEvent<?> event : key.pollEvents()) {
            Future<?> future = handleEvent((Path) event.context(), event.kind());
        }
        key.reset();
        return key;
    }

    private Future<?> handleEvent(final Path path, final WatchEvent.Kind<?> kind) {
        return handlerExecutor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    fileHandler.handleFile(dir.resolve(path).toAbsolutePath(), kind);
                } catch (HandleFileException e) {
                    logger.error(e.getMessage());
                }
            }
        });
    }


    /**
     * Stoppt den FileWatcher, indem es den Thread beendet.
     */
    public void stop() {
        watcherExecutor.shutdownNow();
    }

    /**
     * Fügt ein neues Event zur Event-Liste hinzu.
     *
     * @param event Event, das überwacht werden soll.
     */
    public void addEvent(WatchEvent.Kind event) {
        events.add(event);
    }

    /**
     * Setzt die Events, die vom File-System überwacht werden sollen.
     *
     * @param events ArrayList, die die Events beinhaltet
     */
    public void setEvents(ArrayList<WatchEvent.Kind> events) {
        this.events = events;
    }

    /**
     * Durchsucht das gegebene Verzeichnis auf Dateien und ruft mit diesen
     * handleEvent auf.
     */
    public void searchDirectory() {
        File[] fileEntries = this.dir.toFile().listFiles();
        if (fileEntries != null) {
            for (File fileEntry : fileEntries) {
                handleEvent(fileEntry.toPath().toAbsolutePath(), StandardWatchEventKinds.ENTRY_CREATE);
            }
        }
    }

}