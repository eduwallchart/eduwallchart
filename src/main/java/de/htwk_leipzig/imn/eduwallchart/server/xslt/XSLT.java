package de.htwk_leipzig.imn.eduwallchart.server.xslt;

import java.nio.file.Path;

/**
 * Diese Klasse definiert den Namen eines XSL Templates und den Pfad, an dem es sich befindet
 *
 * @author wreiche on 04.06.14.
 */
public class XSLT {

    private final String name;
    private final Path path;

    /**
     * @param name Name des XSLT
     * @param path Pfad, an dem sich das XSLT befindet
     */
    public XSLT(String name, Path path) {
        this.name = name;
        this.path = path;
    }

    /**
     * @return Getter für den Namen des XSLT
     */
    public String getName() {
        return name;
    }

    /**
     * @return Getter für den Pfad des XSLT
     */
    public Path getPath() {
        return path;
    }

    /**
     * @return Ausgabe des XSLT Objektes als String
     */
    @Override
    public String toString() {
        return name;
    }
}
