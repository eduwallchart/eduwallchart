package de.htwk_leipzig.imn.eduwallchart.server.xslt;

import java.nio.file.Path;

/**
 * Das Interface gibt die Methoden zur Verarbeitung der Templates vor.
 * Dazu gehören das Hinzufügen, Aktualisieren, Entfernen und Zurückgeben der XSLTs.
 *
 * @author Florian Klos
 */
public interface XSLTManager {
    public Iterable<XSLT> getXSLTList();

    public XSLT getDefault();

    public void createOrUpdateXSLT(XSLT xslt) throws DuplicateNameException;

    public void removeXSLT(Path xslt);

    public XSLT getXSLTbyName(String name);
}
