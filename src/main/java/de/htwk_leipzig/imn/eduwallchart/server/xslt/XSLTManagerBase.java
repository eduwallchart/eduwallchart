package de.htwk_leipzig.imn.eduwallchart.server.xslt;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

/**
 * Verwaltet die XSLTs
 *
 * @author wreiche on 04.06.14.
 */
public class XSLTManagerBase implements XSLTManager {

    private final Map<Path, XSLT> xsltMap = newHashMap();
    private XSLT defaultXSLT;

    public XSLTManagerBase() {
        try {
            defaultXSLT = new XSLT("Standard", Paths.get(XSLTManagerBase.class.getResource("default.xslt").toURI()));
            xsltMap.put(defaultXSLT.getPath(), defaultXSLT);
        } catch (URISyntaxException ignored) {
        }
    }

    /**
     * @return gibt alle gespeicherten XSLTs zurück
     */
    @Override
    public Iterable<XSLT> getXSLTList() {
        return xsltMap.values();
    }

    @Override
    public XSLT getDefault() {
        return defaultXSLT;
    }

    /**
     * Speichert ein XSLT, ist der Pfad breits vorhanden, wird es überschrieben
     *
     * @param xslt XSLT Objekt
     */
    @Override
    public synchronized void createOrUpdateXSLT(XSLT xslt) throws DuplicateNameException {
        XSLT savedXSLT = getXSLTbyName(xslt.getName());
        if (savedXSLT == defaultXSLT || savedXSLT.getPath() == xslt.getPath()) {
            xsltMap.put(xslt.getPath(), xslt);
            return;
        }
        throw new DuplicateNameException("Es existiert bereits eine Datei (" + savedXSLT.getPath() + ") mit dem Template-Namen '" + xslt.getName() + "'");
    }

    /**
     * Entfernt ein XSLT
     *
     * @param xslt XSLT Objekt
     */
    @Override
    public void removeXSLT(Path xslt) {
        xsltMap.remove(xslt);
    }

    @Override
    public XSLT getXSLTbyName(String name) {
        for (XSLT xslt : xsltMap.values()) {
            if (xslt.getName().equals(name)) return xslt;
        }
        return defaultXSLT;
    }
}
