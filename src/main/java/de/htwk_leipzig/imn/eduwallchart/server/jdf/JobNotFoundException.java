package de.htwk_leipzig.imn.eduwallchart.server.jdf;

/**
 * Exception, die geworfen wird, wenn ein Link zu einem Job angelegt werden soll, die nicht vorhanden ist
 *
 * @author Georg Felbinger
 */
class JobNotFoundException extends JdfParsingException {

    JobNotFoundException(String description) {
        super(description);
    }
    JobNotFoundException(String description, Exception cause) {
        super(description, cause);
    }
    JobNotFoundException(Exception cause) {
        super(cause);
    }
}
