package de.htwk_leipzig.imn.eduwallchart.server;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import de.htwk_leipzig.imn.eduwallchart.ApplicationEvents;
import de.htwk_leipzig.imn.eduwallchart.ApplicationModule;
import de.htwk_leipzig.imn.eduwallchart.PathModule;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.FileHandler;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.MoveFileHandler;
import de.htwk_leipzig.imn.eduwallchart.server.files.watch.FileWatcher;
import de.htwk_leipzig.imn.eduwallchart.server.files.watch.FileWatcherFactory;
import de.htwk_leipzig.imn.eduwallchart.server.jdf.JdfFileHandler;
import de.htwk_leipzig.imn.eduwallchart.server.xslt.XSLTFileHandler;
import de.htwk_leipzig.imn.eduwallchart.server.xslt.XSLTManager;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

import static de.htwk_leipzig.imn.eduwallchart.PathModule.Jobs;
import static de.htwk_leipzig.imn.eduwallchart.PathModule.Machines;


public class ServiceStarter {
    @Inject ExecutorService executorService;
    @Inject @ApplicationModule.ServerEventBus EventBus eventBus;
    @Inject Logger logger;
    @Inject @PathModule.JdfHotfolder Path hotfolder;
    @Inject @PathModule.JdfSuccessPath Path jdfSuccessPath;
    @Inject @PathModule.JdfFailPath Path jdfFailPath;
    @Inject JdfFileHandler jdfFileHandler;
    @Inject @Machines Path machineXSLTPath;
    @Inject @Jobs Path jobXSLTPath;
    @Inject @Machines XSLTManager machineXsltManager;
    @Inject @Jobs XSLTManager jobXsltManager;

    XSLTFileHandler machineXsltFileHandler;
    XSLTFileHandler jobXsltFileHandler;

    private FileHandler moveJdfFileHandler;
    private FileWatcher jdfFileWatcher;
    private FileWatcher machineXsltFileWatcher;
    private FileWatcher jobXsltFileWatcher;

    /**
     * Startet den FileWatcher auf den Hotfolder-Path
     */
    @PostConstruct
    public void init() throws IOException {
        ArrayList<WatchEvent.Kind> jdfEventList = new ArrayList<>();
        jdfEventList.add(StandardWatchEventKinds.ENTRY_CREATE);
        moveJdfFileHandler = new MoveFileHandler(logger, jdfSuccessPath, jdfFailPath, jdfFileHandler);
        jdfFileWatcher = FileWatcherFactory.createFileWatcher(executorService, moveJdfFileHandler, hotfolder, logger, jdfEventList);
        jdfFileWatcher.start();

        ArrayList<WatchEvent.Kind> xsltEventList = new ArrayList<>();
        xsltEventList.add(StandardWatchEventKinds.ENTRY_MODIFY);
        xsltEventList.add(StandardWatchEventKinds.ENTRY_DELETE);
        machineXsltFileHandler = new XSLTFileHandler(logger, machineXsltManager);
        machineXsltFileWatcher = FileWatcherFactory.createFileWatcher(executorService, machineXsltFileHandler, machineXSLTPath, logger, xsltEventList);
        machineXsltFileWatcher.searchDirectory();
        machineXsltFileWatcher.start();
        jobXsltFileHandler = new XSLTFileHandler(logger, jobXsltManager);
        jobXsltFileWatcher = FileWatcherFactory.createFileWatcher(executorService, jobXsltFileHandler, jobXSLTPath, logger, xsltEventList);
        jobXsltFileWatcher.searchDirectory();
        jobXsltFileWatcher.start();

        eventBus.register(this);
        logger.debug("Services gestartet");
    }

    @Subscribe
    public void recordShutdown(ApplicationEvents.Shutdown shutdown) {
        logger.debug("shutdown services");
        jdfFileWatcher.stop();
        machineXsltFileWatcher.stop();
        jobXsltFileWatcher.stop();
        executorService.shutdown();
    }
}
