package de.htwk_leipzig.imn.eduwallchart.server.xslt;

/**
 * Created by Florian Klos on 12.06.2014.
 */
class DuplicateNameException extends Exception {
    DuplicateNameException(String desc) {
        super(desc);
    }

    DuplicateNameException(String desc, Exception cause) {
        super(desc, cause);
    }

    DuplicateNameException(Exception cause) {
        super(cause);
    }
}
