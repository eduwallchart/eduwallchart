package de.htwk_leipzig.imn.eduwallchart.server;

import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Key;
import com.google.inject.Provides;
import de.htwk_leipzig.imn.eduwallchart.ApplicationModule;
import de.htwk_leipzig.imn.eduwallchart.persistence.JobDao;
import de.htwk_leipzig.imn.eduwallchart.persistence.MachineDao;
import de.htwk_leipzig.imn.eduwallchart.server.jdf.JdfEntityTransaction;
import de.htwk_leipzig.imn.eduwallchart.server.xslt.XSLTManager;
import de.htwk_leipzig.imn.eduwallchart.server.xslt.XSLTManagerBase;
import org.slf4j.Logger;

import static de.htwk_leipzig.imn.eduwallchart.PathModule.Jobs;
import static de.htwk_leipzig.imn.eduwallchart.PathModule.Machines;

/**
 * Das ServerModule stellt Dependencies für das Server package bereit.
 *
 * @author Willi Reiche
 */
public class ServerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ServiceStarter.class).asEagerSingleton();
        bind(Key.get(XSLTManager.class, Machines.class)).to(XSLTManagerBase.class).asEagerSingleton();
        bind(Key.get(XSLTManager.class, Jobs.class)).to(XSLTManagerBase.class).asEagerSingleton();
    }

    @Provides
    @Inject
    private JdfEntityTransaction provideJdfEntityManager(JobDao jobDao, MachineDao machineDao, @ApplicationModule.ServerEventBus EventBus eventBus, Logger logger) {
        return new JdfEntityTransaction(jobDao, machineDao, eventBus, logger);
    }
}
