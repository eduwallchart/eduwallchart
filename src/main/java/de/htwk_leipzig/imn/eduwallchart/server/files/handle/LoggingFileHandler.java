package de.htwk_leipzig.imn.eduwallchart.server.files.handle;

import org.joda.time.DateTime;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.WatchEvent;

/**
 * FileHandler, der einen weiteren FileHandler um die Logging-Funktionalität erweitert (Decorator)
 *
 * @author Georg Felbinger 26.05.14.
 */
public class LoggingFileHandler implements FileHandler {

    private Logger logger;
    private Path logPath;
    private FileHandler fileHandler;

    public LoggingFileHandler(Logger logger, Path logPath, FileHandler fileHandler) {
        this.logger = logger;
        this.logPath = logPath;
        this.fileHandler = fileHandler;
    }


    /**
     * Implementierte Methode aus FileHandler-Interface.
     * Schreibt bei einer HandleFileException ein Log zum in den logPath und wirft die Exception erneut
     *
     * @param pathToFile Zu verarbeitende Datei
     * @throws HandleFileException
     */
    @Override
    public void handleFile(Path pathToFile, WatchEvent.Kind kind) throws HandleFileException {
        String newFileName = DateTime.now().toString() + "_" + pathToFile.toFile().getName();
        newFileName = newFileName.replace(":", "-").replace("+", "_");
        try {
            fileHandler.handleFile(pathToFile, kind);

        } catch (Exception e) {
            logger.error("Parsing failed, writing Log-File to " + logPath.resolve(newFileName), e);
            writeFail(logPath.resolve(newFileName + ".log"), e.getMessage());
            throw new HandleFileException(e);
        }
    }

    private void writeFail(Path file, String msg) {
        try {
            Files.write(file, msg.getBytes(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            logger.error("Unable to write JDFFailLog.", e);
        }
    }

}
