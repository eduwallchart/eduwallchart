package de.htwk_leipzig.imn.eduwallchart.server.jdf;

/**
 * Exception, die während des Parsens von JDFs geworfen werden können, werden hiervon abgeleitet
 *
 * @author Georg Felbinger
 */
class JdfParsingException extends Exception {
    JdfParsingException(String desc) {
        super(desc);
    }

    JdfParsingException(String desc, Exception cause) {
        super(desc, cause);
    }

    JdfParsingException(Exception cause) {
        super(cause);
    }
}
