package de.htwk_leipzig.imn.eduwallchart.server.jdf;

import com.google.common.base.Optional;
import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import de.htwk_leipzig.imn.eduwallchart.persistence.*;
import de.htwk_leipzig.imn.eduwallchart.server.ServerEvents;
import org.joda.time.DateTime;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import java.util.HashMap;

import static com.google.inject.internal.util.$Preconditions.checkNotNull;

/**
 * Der JdfEntityManager sammelt Maschinen, Jobs und Maschinen-Job-Links während des Auslesens einer Datei, und commited / rollbacked diese zum Schluss
 *
 * @author Georg Felbinger
 */
public class JdfEntityTransaction {

    @Inject JobDao jobDao;
    @Inject MachineDao machineDao;
    private EventBus eventBus;
    private Logger logger;
    private HashMap<String, Job> jobs = new HashMap<>();
    private HashMap<String, Machine> machines = new HashMap<>();

    public JdfEntityTransaction() {
    }

    public JdfEntityTransaction(JobDao jobDao, MachineDao machineDao, EventBus eventBus, Logger logger) {
        this.jobDao = jobDao;
        this.machineDao = machineDao;
        this.eventBus = eventBus;
        this.logger = logger;
    }

    @PostConstruct
    void init() {
        checkNotNull(jobDao);
    }

    public void begin(Logger logger) {
        this.logger = logger;
    }

    /**
     * Bei Ausführung der Methode commit() werden die gesammelten Maschinen und Jobs an den JobDAO weitergegeben
     */
    public void commit() {
        logger.debug("Beginn committing...");
        for (Job job : jobs.values()) {
            logger.debug("Commiting Job '" + job.getId() + "'");
            jobDao.insertOrUpdateJob(job);
        }
        for (Machine machine : machines.values()) {
            logger.debug("Commiting Machine '" + machine.getId() + "'");
            machineDao.insertOrUpdateMachine(machine);
        }
        logger.debug("Commit finished.");

        eventBus.post(new ServerEvents.DataChanged(machineDao.getAllMachines()));
        logger.debug("Posted new Machine-List to Clients.");
    }

    /**
     * Bei rollback() werden die Listen verworfen und nicht weitergegeben
     */
    public void rollback() {
        jobs.clear();
        machines.clear();
    }

    /**
     * Erzeugt einen Job und fügt diesen in die Job-Liste hinzu, wenn nicht vorhanden.
     *
     * @param jobId           ID des Jobs
     * @param descriptiveName Beschreibung des Jobs
     * @param jobStatus       Status des Jobs
     * @param startTime       Optionale Soll-Startzeit des Jobs
     * @param endTime         Optionale Soll-Endzeit des Jobs
     */
    public void insertOrUpdateJob(String jobId, String descriptiveName, JobStatus jobStatus, Optional<DateTime> startTime, Optional<DateTime> endTime) {
        jobs.put(jobId, new Job(jobId, descriptiveName, jobStatus, startTime, endTime));
    }

    /**
     * Erzeugt eine Maschine und fügt diesen in die Job-Liste hinzu, wenn nicht vorhanden.
     *
     * @param machineId       ID der Maschine
     * @param descriptiveName Beschreibung des Jobs
     * @param machineStatus   Status des Jobs
     */
    public void insertOrUpdateMachine(String machineId, String descriptiveName, MachineStatus machineStatus) {
        machines.put(machineId, new Machine(machineId, descriptiveName, machineStatus));
    }

    /**
     * Erzeugt einen Link zwischen dem übergebenen Job und der übergegebenen Maschine, wenn diese während des Commits angelegt wurden
     *
     * @param jobId     ID des Jobs
     * @param machineId ID der Maschine
     * @throws MachineNotFoundException Wird geworfen, wenn die übergebene Maschine im Commit nicht vorhanden ist
     * @throws JobNotFoundException     Wird geworfen, wenn der übergebene Job im Commit nicht vorhanden ist
     */
    public void insertMachineJobLink(String jobId, String machineId) throws MachineNotFoundException, JobNotFoundException {
        Optional<Job> job = Optional.fromNullable(jobs.get(jobId));
        Optional<Machine> machine = Optional.fromNullable(machines.get(machineId));

        if (!machine.isPresent()) {
            throw new MachineNotFoundException("There is no machine '" + machineId + "' in this Transaction.");
        } else if (!job.isPresent()) {
            throw new JobNotFoundException("There is no job '" + jobId + "' in this Transaction.");
        } else {
            machines.get(machineId).addJobLink(jobs.get(jobId));
            logger.debug("Link created.");
        }
    }

    /**
     * Setzt nachträglich die (Soll) Start- und Endzeiten eines Jobs
     *
     * @param jobId     ID des Jobs
     * @param startTime Optionale Soll-Startzeit des Jobs
     * @param endTime   Optionale Soll-Endzeit des Jobs
     */
    public void setJobTimes(String jobId, Optional<DateTime> startTime, Optional<DateTime> endTime) {
        Job job = jobs.get(jobId);
        job.setStartTime(startTime);
        job.setEndTime(endTime);
    }

    /**
     * Verknüpft eine Kind-Job mit dem Eltern-Job. Beide Jobs müssen innerhalb eines Commits angelegt werden
     *
     * @param jobId   ID des Eltern-Jobs
     * @param childId ID des Kind-Jobs
     * @throws JobNotFoundException Wird geworfen, wenn entweder Eltern- oder Kind-Job nicht im Commit gefunden werden.
     */
    public void addChildToJob(String jobId, String childId) throws JobNotFoundException {
        Optional<Job> job = Optional.fromNullable(jobs.get(jobId));
        Optional<Job> child = Optional.fromNullable(jobs.get(childId));

        if (!job.isPresent()) {
            throw new JobNotFoundException("There is no job '" + jobId + "' in this Transaction.");
        } else if (!child.isPresent()) {
            throw new JobNotFoundException("There is no child '" + childId + "' in this Transaction.");
        } else {
            job.get().addChild(child.get());
            logger.debug("Child added.");
        }
    }
}
