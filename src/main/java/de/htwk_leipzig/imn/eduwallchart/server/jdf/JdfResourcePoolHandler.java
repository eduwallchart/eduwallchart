package de.htwk_leipzig.imn.eduwallchart.server.jdf;

import com.google.common.base.Optional;
import de.htwk_leipzig.imn.eduwallchart.persistence.MachineStatus;
import org.cip4.jdflib.core.JDFCoreConstants;
import org.cip4.jdflib.core.KElement;
import org.cip4.jdflib.core.VElement;
import org.cip4.jdflib.pool.JDFResourceLinkPool;
import org.cip4.jdflib.pool.JDFResourcePool;
import org.cip4.jdflib.resource.JDFDevice;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;

import java.util.Vector;

/**
 * Created by Georg Felbinger on 22.01.14.
 *
 * @author Georg Felbinger
 *         Review: Florian Klos (19.04.2014)
 *         The JdfResourcePoolHandler handles Machines given by a JdfFileHandler.
 */
class JdfResourcePoolHandler {

    /**
     * Die Methode durchsucht einen JdfResourcePool nach Maschinen und gibt diese an den jdfEntityManager weiter
     *
     * @param jobId
     * @param jdfResourcePool      Zu durchsuchender JdfResourcePool
     * @param jdfEntityTransaction JdfEntityManager, an den die Maschinen weitergegeben werden
     */
    void parseDevices(String jobId, Optional<JDFResourcePool> jdfResourcePool, JdfEntityTransaction jdfEntityTransaction, Logger logger) {
        logger.debug("Start searching for Devices in JdfRessourcePool of Job " + jobId);

        if (jdfResourcePool.isPresent()) {
            Optional<Vector<JDFDevice>> jdfDevices = Optional.fromNullable(jdfResourcePool.get().getChildrenByClass(JDFDevice.class, false, 0));
            if (jdfDevices.isPresent()) {
                for (JDFDevice jdfDevice : jdfDevices.get()) {
                    logger.debug("Machineobject " + jdfDevice.getID() + " created, giving it to jdfEntityManager..");
                    jdfEntityTransaction.insertOrUpdateMachine(jdfDevice.getID(), jdfDevice.getDescriptiveName(), MachineStatus.jdfResStatus2Enum(jdfDevice.getResStatus(false)));
                }
            }
        }
    }

    /**
     * Die Methode parseResourceLinkPool parsed einen Resource-Link-Pool, sofern vorhanden.
     *
     * @param jobId                Id des zum Resource-Link-Pool gehörigen Jobs
     * @param resourceLinkPool     Zu parsender Resource-Link-Pool
     * @param jdfEntityTransaction
     */
    void parseResourceLinkPool(String jobId, Optional<JDFResourceLinkPool> resourceLinkPool, JdfEntityTransaction jdfEntityTransaction, Logger logger) throws JdfParsingException {
        logger.debug("Start searching for DeviceLinks in ResourceLinkPool of Job " + jobId);
        if (resourceLinkPool.isPresent()) {

            resolveDeviceLinks(jobId, Optional.fromNullable(resourceLinkPool.get().getPoolChildren("DeviceLink", null, null)), jdfEntityTransaction, logger);
            resolveNodeInfoLinks(jobId, Optional.fromNullable(resourceLinkPool.get().getLinkedResources("NodeInfo", null, null, false, null)), jdfEntityTransaction, logger);

        }
    }

    /**
     * Die Methode parsed NodeInfos und gibt dem EntityManager die Start und End-Zeiten für den jeweiligen Job weiter
     *
     * @param jobId                Job, zu dem die NodeInfos gehören
     * @param nodeInfos            Zu parsende NodeInfos
     * @param jdfEntityTransaction Entity-Manager, and den die Zeiten weitergegeben werden
     */
    private void resolveNodeInfoLinks(String jobId, Optional<VElement> nodeInfos, JdfEntityTransaction jdfEntityTransaction, Logger logger) {
        if (nodeInfos.isPresent()) {
            for (KElement nodeInfo : nodeInfos.get()) {
                if (nodeInfo.getAttribute("Start") != JDFCoreConstants.EMPTYSTRING && nodeInfo.getAttribute("End") != JDFCoreConstants.EMPTYSTRING) {
                    logger.debug("Setting times of Job " + jobId + "; Start: " + nodeInfo.getAttribute("Start") + ", End: " + nodeInfo.getAttribute("End"));

                    DateTime startTime = DateTime.parse(nodeInfo.getAttribute("Start"), ISODateTimeFormat.dateTimeNoMillis());
                    DateTime endTime = DateTime.parse(nodeInfo.getAttribute("End"), ISODateTimeFormat.dateTimeNoMillis());

                    jdfEntityTransaction.setJobTimes(jobId, Optional.fromNullable(startTime), Optional.fromNullable(endTime));
                }
            }
        }
    }

    /**
     * Erstellt aus einem Vektor aus Device-Links die Beziehungen zwischen Jobs und Maschinen
     *
     * @param jobId                Zugehörige Job-Id zu den Device-Links
     * @param deviceLinks          Vektor aus Device-Links
     * @param jdfEntityTransaction Entity-Manager, and den die Device-Job-Links weitergegeben werden
     */
    private void resolveDeviceLinks(String jobId, Optional<VElement> deviceLinks, JdfEntityTransaction jdfEntityTransaction, Logger logger) throws JobNotFoundException, MachineNotFoundException {
        if (deviceLinks.isPresent()) {
            for (KElement deviceLink : deviceLinks.get()) {
                logger.debug("Creating Link between " + jobId + " and Device " + deviceLink.getAttribute("rRef"));
                jdfEntityTransaction.insertMachineJobLink(jobId, deviceLink.getAttribute("rRef"));
            }
        }
    }
}
