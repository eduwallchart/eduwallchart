package de.htwk_leipzig.imn.eduwallchart.server;

import de.htwk_leipzig.imn.eduwallchart.persistence.Machine;

import java.util.List;

/**
 * Utility-Klasse für Events aus dem Server-Package
 *
 * @author Georg Felbinger, 14.05.2014.
 */
public abstract class ServerEvents {
    public static class DataChanged {
        private final List<Machine> machineList;

        public DataChanged(List<Machine> machineList) {
            this.machineList = machineList;
        }

        public List<Machine> getMachineList() {
            return machineList;
        }
    }
}
