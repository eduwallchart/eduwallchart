package de.htwk_leipzig.imn.eduwallchart.server.files.handle;

import org.joda.time.DateTime;
import org.slf4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchEvent;

/**
 * FileHandler, der einen weiteren FileHandler um die Funktionalität erweitert, verarbeitete Dateien zu verschieben (Decorator)
 *
 * @author Georg Felbinger 26.05.14.
 */
public class MoveFileHandler implements FileHandler {

    private Logger logger;
    private FileHandler fileHandler;
    private Path successPath;
    private Path failPath;

    public MoveFileHandler(Logger logger, Path successPath, Path failPath, FileHandler fileHandler) {
        this.logger = logger;
        this.fileHandler = fileHandler;
        this.successPath = successPath;
        this.failPath = failPath;
    }

    /**
     * Implementierte Methode aus FileHandler-Interface.
     * Verschiebt die Datei nach dem Verarbeiten je nach Erfolg nach failPath oder successPath.
     *
     * @param pathToFile Zu verarbeitende Datei
     * @throws HandleFileException
     */
    @Override
    public void handleFile(Path pathToFile, WatchEvent.Kind kind) throws HandleFileException {
        String newFileName = DateTime.now().toString() + "_" + pathToFile.toFile().getName();
        newFileName = newFileName.replace(":", "-").replace("+","_");
        try {
            fileHandler.handleFile(pathToFile, kind);
            logger.debug("Parsing successful, moving File from " + pathToFile.toString() + " to " + successPath.resolve(newFileName));
            moveFile(pathToFile, successPath.resolve(newFileName));
        } catch (HandleFileException e) {
            logger.error("Parsing failed, moving File to " + failPath.resolve(newFileName), e);
            moveFile(pathToFile, failPath.resolve(newFileName));
            throw new HandleFileException(e);
        }
    }

    private void moveFile(Path from, Path to) {
        try {
            Files.move(from, to);
        } catch (IOException e) {
            logger.error("Unable to move JDF.", e);
        }
    }
}
