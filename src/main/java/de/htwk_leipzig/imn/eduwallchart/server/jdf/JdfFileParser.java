package de.htwk_leipzig.imn.eduwallchart.server.jdf;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Provider;
import de.htwk_leipzig.imn.eduwallchart.persistence.JobStatus;
import org.cip4.jdflib.core.JDFDoc;
import org.cip4.jdflib.core.JDFElement;
import org.cip4.jdflib.node.JDFNode;
import org.joda.time.DateTime;
import org.slf4j.Logger;

import java.nio.file.Path;
import java.util.Vector;

import static com.google.inject.internal.util.$Preconditions.checkNotNull;

/**
 * Klasse zum Behandeln von JDF-Knoten
 *
 * @author Georg Felbinger, 27.05.2014
 */
class JdfFileParser {

    @Inject JdfResourcePoolHandler jdfResourcePoolHandler;
    @Inject Provider<JdfEntityTransaction> jdfEntityManager;
    @Inject Logger injectLogger;

    /**
     * Die Methode importiert die übergebene Datei und übergibt den enthaltenen ResourcePool an parseJdfResourcePool()
     *
     * @param jdfFile Pfad zur JDF-Datei, welche verarbeitet werden soll
     */
    void parseJdfFile(Path jdfFile, Logger logger) throws JdfParsingException {
        logger.debug("Start parsing the File:" + jdfFile.toAbsolutePath().toString());
        JdfEntityTransaction manager = jdfEntityManager.get();
        checkNotNull(jdfFile);
        Optional<JDFDoc> jdfDoc = Optional.fromNullable(JDFDoc.parseFile(jdfFile.toFile()));
        if (jdfDoc.isPresent()) {
            Optional<JDFNode> jdfRoot = Optional.fromNullable(jdfDoc.get().getJDFRoot());
            if (jdfRoot.isPresent()) {
                manager.begin(logger);
                parseJdfNode(jdfRoot.get(), manager, logger);
            } else {
                throw new JdfParsingException("Could not get JDF-Root, PrintTalkJ brought Null.");
            }
        } else {
            throw new JdfParsingException("Could not parse Document, PrintTalkJ brought Null.");
        }
        manager.commit();
    }

    /**
     * Die Methode parsJdfNode verarbeitet rekursiv den übergebenen JDF-Knoten und alle darin enthaltenen Kind-JDF-Knoten.
     *
     * @param node Jdf-Knoten, der verarbeitet werden soll.
     */
    private void parseJdfNode(JDFNode node, JdfEntityTransaction jdfEntityTransaction, Logger logger) throws JdfParsingException {
        String jobId = node.getID();
        logger.debug("Create Job:" + jobId);

        Optional<JDFElement.EnumNodeStatus> jdfStatus = Optional.fromNullable(node.getStatus());
        if (!jdfStatus.isPresent()) throw new JobStatusNotFoundException("Unknown Status in Job " + jobId);

        jdfEntityTransaction.insertOrUpdateJob(jobId, node.getDescriptiveName(), JobStatus.jdfNodeStatus2Enum(jdfStatus.get()), Optional.<DateTime>absent(), Optional.<DateTime>absent());

        jdfResourcePoolHandler.parseDevices(jobId, Optional.fromNullable(node.getResourcePool()), jdfEntityTransaction, logger);
        jdfResourcePoolHandler.parseResourceLinkPool(jobId, Optional.fromNullable(node.getResourceLinkPool()), jdfEntityTransaction, logger);

        Optional<Vector<JDFNode>> childNodes = Optional.fromNullable(node.getChildrenByClass(JDFNode.class, false, 0));

        for (JDFNode child : childNodes.or(new Vector<JDFNode>())) {
            parseJdfNode(child, jdfEntityTransaction, logger);
            logger.debug("Adding Child '" + child.getID() + "' to Job '" + node.getID() + "'...");
            jdfEntityTransaction.addChildToJob(node.getID(), child.getID());
        }
    }
}
