package de.htwk_leipzig.imn.eduwallchart.server.jdf;

/**
 * @author Georg Felbinger 19.05.2014
 */
class JobStatusNotFoundException extends JdfParsingException {

    JobStatusNotFoundException(String desc) {
        super(desc);
    }

    JobStatusNotFoundException(String desc, Exception cause) {
        super(desc, cause);
    }

    JobStatusNotFoundException(Exception cause) {
        super(cause);
    }
}
