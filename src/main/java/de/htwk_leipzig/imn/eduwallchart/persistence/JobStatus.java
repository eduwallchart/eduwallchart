package de.htwk_leipzig.imn.eduwallchart.persistence;

import org.cip4.jdflib.core.JDFConstants;
import org.cip4.jdflib.core.JDFElement.EnumNodeStatus;

/**
 * Created by Georg Felbinger on 06.05.14.
 * Hierbei handelt es sich um die Aufzählung aller möglichen Job-Zustände.
 */
public enum JobStatus {
    WAITING, TESTRUNINPROGRESS, READY, FAILEDTESTRUN, SETUP, INPROGRESS, CLEANUP, SPAWNED, SUSPENDED, STOPPED, COMPLETED, ABORTED, PART, POOL;


    public static JobStatus jdfNodeStatus2Enum(EnumNodeStatus jdfNodeStatus) {
        String jdfStatusName = jdfNodeStatus.getName();

        switch (jdfStatusName) {
            case JDFConstants.WAITING:
                return WAITING;
            case JDFConstants.TESTRUNINPROGRESS:
                return TESTRUNINPROGRESS;
            case JDFConstants.READY:
                return READY;
            case JDFConstants.FAILEDTESTRUN:
                return FAILEDTESTRUN;
            case JDFConstants.SETUP:
                return SETUP;
            case JDFConstants.INPROGRESS:
                return INPROGRESS;
            case JDFConstants.CLEANUP:
                return CLEANUP;
            case JDFConstants.SPAWNED:
                return SPAWNED;
            case JDFConstants.SUSPENDED:
                return SUSPENDED;
            case JDFConstants.STOPPED:
                return STOPPED;
            case JDFConstants.COMPLETED:
                return COMPLETED;
            case JDFConstants.ABORTED:
                return ABORTED;
            case JDFConstants.PART:
                return PART;
            case JDFConstants.POOL:
                return POOL;
            default:
                throw new IllegalArgumentException("There's no Job-Status: '" + jdfStatusName + "'");
        }
    }
}
