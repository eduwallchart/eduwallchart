package de.htwk_leipzig.imn.eduwallchart.persistence;

import org.cip4.jdflib.resource.JDFResource;

import static com.google.inject.internal.util.$Preconditions.checkNotNull;

/**
 * Hierbei handelt es sich um die Aufzählung aller möglichen Maschinen-Zustände.
 *
 * @author Florian Klos
 */

public enum MachineStatus {

    AVAILABLE, COMPLETE, DRAFT, INCOMPLETE, INUSE, REJECTED, STATE_UNAVAILABLE, UNAVAILABLE;

    /**
     * Definition der Konstanten des "enum"s der JDF-Stati, benötigt zur Umwandlung in ewc-Status
     */
    private static final int JDF_INCOMPLETE = 0;
    private static final int JDF_REJECTED = 1;
    private static final int JDF_UNAVAILABLE = 2;
    private static final int JDF_INUSE = 3;
    private static final int JDF_DRAFT = 4;
    private static final int JDF_COMPLETE = 5;
    private static final int JDF_AVAILABLE = 6;

    /**
     * Die Methode wandelt einen JdfStatus in ein Status-Enum um und gibt diesen zurück
     *
     * @param jdfStatus Umzuwandelnder JdfStatus
     * @return Erzeugter Status-Enum
     */
    public static MachineStatus jdfResStatus2Enum(JDFResource.EnumResStatus jdfStatus) {
        MachineStatus ewcStatus;

        switch (checkNotNull(jdfStatus.getValue())) {
            case JDF_INCOMPLETE:
                ewcStatus = MachineStatus.INCOMPLETE;
                break;
            case JDF_REJECTED:
                ewcStatus = MachineStatus.REJECTED;
                break;
            case JDF_UNAVAILABLE:
                ewcStatus = MachineStatus.UNAVAILABLE;
                break;
            case JDF_INUSE:
                ewcStatus = MachineStatus.INUSE;
                break;
            case JDF_DRAFT:
                ewcStatus = MachineStatus.DRAFT;
                break;
            case JDF_COMPLETE:
                ewcStatus = MachineStatus.COMPLETE;
                break;
            case JDF_AVAILABLE:
                ewcStatus = MachineStatus.AVAILABLE;
                break;
            default:
                ewcStatus = MachineStatus.STATE_UNAVAILABLE;
        }
        return ewcStatus;
    }
}
