package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Charsets;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import de.htwk_leipzig.imn.eduwallchart.PathModule;
import org.slf4j.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Das PersistenceModule stellt Dependencies für das Persistence package bereit.
 *
 * @author Willi Reiche
 *         Reviewer: Georg Felbinger
 */
public class PersistenceModule extends AbstractModule {

    public PersistenceModule() {

    }

    @Override
    protected void configure() {
        bind(MachineDao.class).to(MachineDaoBase.class).asEagerSingleton();
        bind(JobDao.class).to(JobDaoBase.class).asEagerSingleton();
    }

    @Provides
    @Singleton
    @Inject
    public EntityManager provideEntityManager(@PathModule.Home Path home, Logger logger) throws Exception {
        Properties properties = new Properties();
        Path propertiesPath = home.resolve("database.properties");
        try {
            properties.load(Files.newBufferedReader(propertiesPath, Charsets.UTF_8));
            return Persistence.createEntityManagerFactory("ewchart", properties).createEntityManager();
        } catch (IOException e) {
            logger.error("Can't read database.properties. Does this file exist and do you have enough rights to read it?");
            throw e;
        } catch (Exception e) {
            logger.error("Error while connecting to database.");
            throw e;
        }
    }
}
