package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Optional;
import org.joda.time.DateTime;
import org.joda.time.Seconds;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Die Job-Klasse beschreibt ein Job-Objekt
 *
 * @author: Peter Nancke
 * Review 1822: Sven Altner
 */
@Entity
@XmlRootElement(name = "job")
@XmlAccessorType(XmlAccessType.FIELD)
public class Job {

    @Id
    private String id;
    private String name;
    private JobStatus status;
    @Convert(converter = JPADateTimeAdapter.class)
    @XmlElement(name = "xmlStartTime") @XmlJavaTypeAdapter(JAXBDateTimeAdapter.class)
    private Optional<DateTime> startTime;
    @Convert(converter = JPADateTimeAdapter.class)
    @XmlElement(name = "xmlEndTime") @XmlJavaTypeAdapter(JAXBDateTimeAdapter.class)
    private Optional<DateTime> endTime;

    @OneToMany
    private List<Job> childs;

    /**
     * leerer Konstruktor
     */
    protected Job() {

    }

    /**
     * Kontruktor, dem die Eigenschaften des Jobs übergeben werden
     *
     * @param id        ID des Jobs
     * @param name      Name des Jobs
     * @param status    Status des Jobs
     * @param startTime Startzeit des Jobs
     * @param endTime   Endzeit des Jobs
     */
    public Job(String id, String name, JobStatus status, Optional<DateTime> startTime, Optional<DateTime> endTime) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.startTime = startTime;
        this.endTime = endTime;
        this.childs = new ArrayList<Job>();
    }

    /**
     * getId() liefert die ID des Jobs
     *
     * @return id vom Job
     */
    public String getId() {
        return id;
    }

    /**
     * setId() setzt die ID von dem Job
     *
     * @param id ID des Jobs
     */
    public void setId(String id) {
        this.id = id;
    }


    /**
     * getName() gibt den Namen des Jobs zurück
     *
     * @return Namen des Jobs
     */
    public String getName() {
        return name;
    }

    /**
     * setName() gibt Namen des Jobs zurück
     *
     * @param name des Jobs
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getStatus() gibt Status des Jobs zurück
     *
     * @return status des Jobs
     */
    public JobStatus getStatus() {
        return status;
    }

    /**
     * setStatus() setzt Status des Jobs
     *
     * @param status Status des Jobs
     */
    public void setStatus(JobStatus status) {
        this.status = status;
    }

    /**
     * getStartTime(Status) liefert Startzeit des Jobs
     *
     * @return Startzeit des Jobs in Format DateTime (siehe Joda Time)
     */
    public Optional<DateTime> getStartTime() {
        return startTime;
    }

    /**
     * setStartTime(DateTime) setzt Startzeit des Jobs
     *
     * @param startTime Startzeit des Jobs
     */
    public void setStartTime(Optional<DateTime> startTime) {
        this.startTime = startTime;
    }

    /**
     * getEndTime() liefert die Endzeit des Jobs
     *
     * @return Endzeit des Jobs
     */
    public Optional<DateTime> getEndTime() {
        return endTime;
    }

    /**
     * setEndTime(DateTime) setzt die Endzeit des Jobs
     *
     * @param endTime Endzeit des Jobs
     */
    public void setEndTime(Optional<DateTime> endTime) {
        this.endTime = endTime;
    }

    /**
     * getTimeProgress liefert den Fortschritt des Jobs entsprechend der verstrichenen Zeit(!)
     *
     * @return Optional des Fortschritts als Int (0..100), wenn kein Start- o. Enddatum dann Absent
     */
    public Optional<Integer> getTimeProgress() {
        if (endTime.isPresent() && startTime.isPresent()) {
            DateTime now = DateTime.now();
            int progress;
            if (now.compareTo(endTime.get()) > 0) {
                progress = 100;
            } else if (now.compareTo(startTime.get()) < 0) {
                progress = 0;
            } else {
                float percent = Seconds.secondsBetween(now.toDateTime(), startTime.get().toDateTime()).getSeconds() * 100 / Seconds.secondsBetween(endTime.get().toDateTime(), startTime.get().toDateTime()).getSeconds();
                progress = Math.round(percent);
            }
            return Optional.of(progress);
        } else {
            return Optional.absent();
        }
    }

    public void addChild(Job child) {
        childs.add(child);
    }

}
