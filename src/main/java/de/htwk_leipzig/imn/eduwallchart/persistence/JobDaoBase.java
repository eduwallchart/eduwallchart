package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import org.joda.time.DateTime;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

/**
 * Implementiert das Interface JobDao und persistiert die Jobs mittels JPA.
 *
 * @author Florian Klos
 */
public class JobDaoBase implements JobDao {

    @Inject
    EntityManager manager;

    /**
     * Erstellt einen neuen Job, wenn die ID unbekannt ist.
     * Wenn die ID bereits existiert, wird der entsprechende Job aktualisiert.
     *
     * @param job Job
     * @return persistierter Job
     */
    @Override
    public Job insertOrUpdateJob(Job job) {
        EntityTransaction transaction = manager.getTransaction();
        transaction.begin();
        manager.merge(job);
        transaction.commit();
        return job;
    }

    /**
     * Erstellt einen neuen Job, wenn die ID unbekannt ist.
     * Wenn die ID bereits existiert, wird der entsprechende Job aktualisiert.
     *
     * @param id    ID des Jobs
     * @param name  Name des Jobs
     * @param state Status des Jobs
     * @return persistierter Job
     */
    @Override
    public Job createOrUpdateJob(String id, String name, JobStatus state, Optional<DateTime> startTime, Optional<DateTime> endTime) {
        Optional<Job> existingJob = getJob(id);
        Job currentJob = new Job(id, name, state, startTime, endTime);
        if (!existingJob.isPresent()) {
            return createJob(currentJob);
        }
        return updateJob(currentJob);
    }

    /**
     * Schreibt einen neuen Job in die Datenbank.
     *
     * @param job zu speichernder Job
     * @return persistierter Job
     */
    public Job createJob(Job job) {
        EntityTransaction transaction = manager.getTransaction();
        transaction.begin();
        manager.persist(job);
        transaction.commit();
        return job;
    }

    /**
     * Aktualisiert einen Job in der Datenbank.
     *
     * @param job zu speichernder Job
     * @return persistierter Job
     */
    public Job updateJob(Job job) {
        EntityTransaction transaction = manager.getTransaction();
        transaction.begin();
        manager.merge(job);
        transaction.commit();
        return job;
    }

    /**
     * Sucht den Job mit entsprechender ID und gibt diesen zurück.
     *
     * @param id ID des gewünschten Jobs
     * @return entsprechender Job (als Optional)
     */
    @Override
    public Optional<Job> getJob(String id) {
        Query query = manager.createQuery("SELECT j FROM Job j WHERE j.id = :id");
        query.setParameter("id", id);
        List<Job> results = query.getResultList();
        try {
            return Optional.fromNullable(results.get(0));
        } catch (IndexOutOfBoundsException e) {
            return Optional.fromNullable(null);
        }
    }

    /**
     * Gibt alle Jobs der Datenbank zurück.
     *
     * @return Liste aller existierenden Jobs
     */
    @Override
    public List<Job> getAllJobs() {
        Query query = manager.createQuery("SELECT j FROM Job j ORDER BY j.startTime");
        return query.getResultList();
    }
}
