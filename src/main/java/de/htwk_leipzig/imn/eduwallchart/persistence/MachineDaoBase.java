package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

/**
 * MachineDaoBase kümmert sich um das Einpflegen der Maschinen-Objekte in die Datenbank.
 * Dafür wird JPA verwendet.
 *
 * @author Florian Klos
 *         Review 1669: Peter Nancke
 *         Review 1949: Willi Reiche
 */
@Singleton
public class MachineDaoBase implements MachineDao {

    @Inject EntityManager manager;

    /**
     * insertOrUpdateMachine wird zum Persistieren einer Maschine verwendet.
     * In dieser Methode wird entschieden, ob die Maschine völlig neu angelegt wird,
     * oder ob bereits eine Maschine mit der entsprechenden ID existiert und diese
     * geupdated werden muss.
     *
     * @param machine Maschine
     * @return persistierte Maschine
     */
    @Override
    public Machine insertOrUpdateMachine(Machine machine) {
        EntityTransaction transaction = manager.getTransaction();
        transaction.begin();
        manager.merge(machine);
        transaction.commit();
        return machine;
    }

    @Override
    /**
     * insertOrUpdateMachine erstellt oder updated eine Maschine.
     * Ist bereits eine Maschine mit entsprechende ID vorhanden wird updateMachine aufgerufen,
     * wenn nicht dann createMachine
     * @param id    ID der Maschine
     * @param name  Name der Maschine
     * @param state aktueller Status der Maschine
     * @return persistierte Maschine
     */
    public Machine createOrUpdateMachine(String id, String name, MachineStatus state) {
        Optional<Machine> machine = getMachine(id);
        if (!machine.isPresent()) {
            return createMachine(id, name, state);
        }
        return updateMachine(new Machine(id, name, state));
    }

    @Override
    /**
     * createMachine pflegt eine neue Maschine in die Datenbank ein.
     * @param id        ID der Maschine
     * @param name        Name der Maschine
     * @param state    aktueller Status der Maschine
     */
    public Machine createMachine(String id, String name, MachineStatus state) {
        EntityTransaction transaction = manager.getTransaction();
        transaction.begin();
        Machine machine = new Machine(id, name, state);
        manager.persist(machine);
        transaction.commit();
        return machine;
    }

    @Override
    /**
     * updateMachine aktualisiert eine Maschine der Datenbank
     * @param machine   Maschinen-Objekt mit aktuelleren Werten
     * @return persistierte Maschine
     */
    public Machine updateMachine(Machine machine) {
        EntityTransaction transaction = manager.getTransaction();
        transaction.begin();
        manager.merge(machine);
        transaction.commit();
        return machine;
    }

    @SuppressWarnings("unchecked")
    @Override
    /**
     * getMachine liefert eine oder keine Maschine als Optional zurück.
     * Die Maschine wird über die ID der Maschine gesucht.
     * @param id    ID der gesuchten Maschine
     * @return Optional<Machine>
     */
    public Optional<Machine> getMachine(String id) {
        Query query = manager.createQuery("SELECT m FROM Machine m WHERE m.id = :id");
        query.setParameter("id", id);
        List<Machine> results = query.getResultList();
        try {
            return Optional.fromNullable(results.get(0));
        } catch (IndexOutOfBoundsException e) {
            return Optional.fromNullable(null);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    /**
     * getAllMachines liefert eine Liste aller vorhandenen Maschinen zurück.
     * @return Liste aller Maschinen
     */
    public List<Machine> getAllMachines() {
        Query query = manager.createQuery("SELECT m FROM Machine m");
        return query.getResultList();
    }
}
