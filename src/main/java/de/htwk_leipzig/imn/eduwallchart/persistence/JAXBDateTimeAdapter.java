package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Optional;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Klasse zur Konvertierung von Optional<DateTime> nach String und zurück für JAXB
 *
 * @author Georg Felbinger, 25.06.2014
 */
public class JAXBDateTimeAdapter extends XmlAdapter<String, Optional<DateTime>> {

    public JAXBDateTimeAdapter() {
    }

    /**
     * Konvertierung für JAXB: String -> Optional<DateTime> (Im Format "dd.MM.YYYY, HH:mm")
     *
     * @param v Zu konvertierender String
     * @return Optional<DateTime> wenn erfolgreich geparsed wird, sonst Optional.absent()
     * @throws Exception Vererbt aus XmlAdapter, nicht benötigt...
     */
    @Override
    public Optional<DateTime> unmarshal(String v) throws Exception {
        return Optional.fromNullable(DateTime.parse(v, DateTimeFormat.forPattern("dd.MM.YYYY, HH:mm")));
    }

    /**
     * Konvertierung für JAXB: Optional<DateTime> -> String (Im Format "dd.MM.YYYY, HH:mm")
     *
     * @param v Zu konvertierendes Optional
     * @return Konvertierter String, bei Optional.absent() leerer String
     * @throws Exception Vererbt aus XmlAdapter, nicht benötigt...
     */
    @Override
    public String marshal(Optional<DateTime> v) throws Exception {
        return v.isPresent() ? v.get().toString("dd.MM.YYYY, HH:mm") : "";
    }
}