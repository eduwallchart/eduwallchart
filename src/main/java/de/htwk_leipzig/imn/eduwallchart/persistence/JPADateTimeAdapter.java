package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import org.joda.time.DateTime;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Klasse zur Konvertierung von Optional<DateTime> nach String und zurück für JPA
 *
 * @author Georg Felbinger, 25.06.2014
 */
@Converter
public class JPADateTimeAdapter implements AttributeConverter<Optional<DateTime>, String> {
    /**
     * Konvertierung für JPA: Optional<DateTime> -> String (Im Standard-Joda-Format)
     *
     * @param dateTimeOptional Zu konvertierendes Optional
     * @return Konvertierter String, bei Optional.absent() leerer String
     */
    @Override
    public String convertToDatabaseColumn(Optional<DateTime> dateTimeOptional) {
        return dateTimeOptional.isPresent() ? dateTimeOptional.get().toString() : null;
    }

    /**
     * Konvertierung für JPA: String -> Optional<DateTime> (Im Standard-Joda-Format)
     *
     * @param s Zu konvertierender String
     * @return Optional<DateTime> wenn erfolgreich geparsed wird, sonst Optional.absent()
     */
    @Override
    public Optional<DateTime> convertToEntityAttribute(String s) {
        try {
            return Optional.fromNullable(s).transform(new Function<String, DateTime>() {
                @Override
                public DateTime apply(String input) {
                    return DateTime.parse(input);
                }
            });
        } catch (IllegalArgumentException | UnsupportedOperationException e) {
            return Optional.absent();
        }
    }
}
