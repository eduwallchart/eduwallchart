package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Optional;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Das Interface JobDao schreibt die Methoden für die Verarbeitung der Jobs
 * in der Datenbank vor.
 *
 * @author Florian Klos
 */
public interface JobDao {

    /**
     * Erstellt einen neuen Job, wenn die ID unbekannt ist.
     * Wenn die ID bereits existiert, wird der entsprechende Job aktualisiert.
     *
     * @param job Job
     * @return persistierter Job
     */
    public Job insertOrUpdateJob(Job job);

    /**
     * Erstellt einen neuen Job, wenn die ID unbekannt ist.
     * Wenn die ID bereits existiert, wird der entsprechende Job aktualisiert.
     *
     * @param id    ID des Jobs
     * @param name  Name des Jobs
     * @param state Status des Jobs
     * @return persistierter Job
     */
    public Job createOrUpdateJob(String id, String name, JobStatus state, Optional<DateTime> startTime, Optional<DateTime> endTime);

    /**
     * Sucht den Job mit entsprechender ID und gibt diesen zurück.
     *
     * @param id ID des gewünschten Jobs
     * @return entsprechender Job (als Optional)
     */
    public Optional<Job> getJob(String id);

    /**
     * Gibt alle Jobs der Datenbank zurück.
     *
     * @return Liste aller existierenden Jobs
     */
    public List<Job> getAllJobs();

}