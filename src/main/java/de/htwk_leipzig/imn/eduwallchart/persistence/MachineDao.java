package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Optional;

import java.util.List;

/**
 * MachineDao Interface
 * Das Interface gibt die Methoden für die Einpflegung der Maschinen in die Datenbank vor.
 * Somit sollen sowohl neue Maschinen hinzugefügt, sowie einzelne oder alle Maschinen zurückgegeben werden.
 *
 * @author Florian Klos
 *         Review: 1647 Willi Reiche
 */
public interface MachineDao {

    /**
     * insertOrUpdateMachine wird zum Persistieren einer Maschine verwendet.
     * In dieser Methode wird entschieden, ob die Maschine völlig neu angelegt wird,
     * oder ob bereits eine Maschine mit der entsprechenden ID existiert und diese
     * geupdated werden muss.
     *
     * @param machine Maschine
     * @return persistierte Maschine
     */
    public Machine insertOrUpdateMachine(Machine machine);

    /**
     * insertOrUpdateMachine wird zum Persistieren einer Maschine verwendet.
     * In dieser Methode wird entschieden, ob die Maschine völlig neu angelegt wird,
     * oder ob bereits eine Maschine mit der entsprechenden ID existiert und diese
     * geupdated werden muss. Je nach Fall wird createMachine oder updateMachine aufgerufen.
     *
     * @param id    ID der Maschine
     * @param name  Name der Maschine
     * @param state aktueller Status der Maschine
     * @return persistierte Maschine
     */
    public Machine createOrUpdateMachine(String id, String name, MachineStatus state);

    /**
     * createMachine soll eine neue Maschine in die Datenbank einpflegen.
     *
     * @param id    ID der Maschine
     * @param name  Name der Maschine
     * @param state aktueller Status der Maschine
     * @return persistierte Maschine
     */
    public Machine createMachine(String id, String name, MachineStatus state);

    /**
     * updateMachine verändert die Werte eines bereits vorhandenen Maschinen-Objekts.
     * Der Schlüssel ist dabei die ID. Diese bleibt in jedem Fall gleich.
     *
     * @param machine Maschinen-Objekt mit aktuelleren Werten
     * @return persistierte Maschine
     */
    public Machine updateMachine(Machine machine);

    /**
     * getMachine soll ein Maschinen-Objekt zurückgeben.
     * Übergeben wird die ID der gesuchten Maschine.
     *
     * @param id ID der gewollten Maschine
     * @return Maschinen-Objekt
     */
    public Optional<Machine> getMachine(String id);

    /**
     * getAllMachines liefert alle vorhandenen Maschinen zurück.
     *
     * @return List mit allen, in der Datenbank vorhandenen, Maschinen
     */
    public List<Machine> getAllMachines();

}