package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Die Klasse Machine definiert ein Maschinenobjekt
 *
 * @author Willi Reiche
 *         Review 1654: Peter Nancke
 */
@Entity
public class Machine {

    @Id
    private String id;
    private String name;
    private MachineStatus machineStatus;

    @OneToMany
    private List<Job> jobs;

    Machine() {

    }

    /**
     * Standard Konstruktor
     *
     * @param id            ID der Maschine
     * @param name          Name der Maschine
     * @param machineStatus aktueller Status der Maschine
     */
    public Machine(String id, String name, MachineStatus machineStatus) {
        this.id = id;
        this.name = name;
        this.machineStatus = machineStatus;
        this.jobs = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MachineStatus getMachineStatus() {
        return machineStatus;
    }

    public void setMachineStatus(MachineStatus machineStatus) {
        this.machineStatus = machineStatus;
    }

    public void addJobLink(Job linkedJob) {
        jobs.add(linkedJob);
    }

    public List<Job> getJobs() {
        return this.jobs;
    }

    /**
     * toString gibt die Felder eines Maschinenobjektes in einem String aus
     */
    @Override
    public String toString() {
        return Objects.toStringHelper(this).add("ID: ", id).add("Name: ", name)
                .add("Status: ", machineStatus).toString();
    }
}
