package de.htwk_leipzig.imn.eduwallchart.ui;

import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;
import de.htwk_leipzig.imn.eduwallchart.ui.util.CookieManager;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UserSettings;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Das UIModule stellt Dependencies für das UI package bereit.
 *
 * @author Willi Reiche
 */
public class UIModule extends AbstractModule {
    @Override
    protected void configure() {

    }

    @Provides
    private CookieManager provideCookieManager() {
        return EduWallChartUI.getCurrentCookieManager();
    }

    @Provides
    private UserSettings provideUserSettings() {
        return EduWallChartUI.getCurrentUserSettings();
    }

    @Provides
    @SessionEventBus
    private EventBus provideSessionEventBus() {
        return EduWallChartUI.getCurrentSessionEventBus();
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface SessionEventBus {
    }
}
