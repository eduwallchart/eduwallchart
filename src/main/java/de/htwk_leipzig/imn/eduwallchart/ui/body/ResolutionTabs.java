package de.htwk_leipzig.imn.eduwallchart.ui.body;

import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeButton;
import de.htwk_leipzig.imn.eduwallchart.ui.util.TimeLineResolution;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UIEvents;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UserSettings;

/**
 * @author Georg Felbinger, 13.06.14.
 */
public class ResolutionTabs extends HorizontalLayout {

    private BiMap<TimeLineResolution, NativeButton> resolutionButtons;
    private UserSettings userSettings;

    public ResolutionTabs(UserSettings userSettings, EventBus sessionEventBus) {
        this.userSettings = userSettings;

        resolutionButtons = createResolutionButtonMap();
        this.addComponent(resolutionButtons.get(TimeLineResolution.DAY));
        this.addComponent(resolutionButtons.get(TimeLineResolution.WEEK));
        this.addComponent(resolutionButtons.get(TimeLineResolution.MONTH));

        if (!userSettings.isCustomResolution()) {
            setActive(userSettings.getTimeLineResolution());
        }

        sessionEventBus.register(this);
    }

    private BiMap<TimeLineResolution, NativeButton> createResolutionButtonMap() {
        BiMap<TimeLineResolution, NativeButton> resolutionButtons = EnumHashBiMap.create(TimeLineResolution.class);
        resolutionButtons.put(TimeLineResolution.DAY, createResolutionButton("Tag"));
        resolutionButtons.put(TimeLineResolution.WEEK, createResolutionButton("Woche"));
        resolutionButtons.put(TimeLineResolution.MONTH, createResolutionButton("Monat"));
        return resolutionButtons;
    }

    private NativeButton createResolutionButton(String name) {
        NativeButton button = new NativeButton(name);
        button.addStyleName("tab");
        button.addClickListener(createResolutionClickListener());
        return button;
    }

    private Button.ClickListener createResolutionClickListener() {
        return new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                userSettings.setTimeLineResolution(resolutionButtons.inverse().get(event.getButton()));
            }
        };
    }

    @Subscribe
    public void setActiveButton(UIEvents.UserSettingsChangeEvent event) {
        if (!userSettings.isCustomResolution()) {
            setActive(userSettings.getTimeLineResolution());
        } else {
            setAllUnactive();
        }
    }

    private void setAllUnactive() {
        for (NativeButton button : resolutionButtons.values()) {
            button.removeStyleName("active");
        }
    }

    private void setActive(final TimeLineResolution timeLineResolution) {
        setAllUnactive();
        resolutionButtons.get(timeLineResolution).addStyleName("active");
    }
}
