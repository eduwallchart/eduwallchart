package de.htwk_leipzig.imn.eduwallchart.ui.body;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.NativeButton;
import de.htwk_leipzig.imn.eduwallchart.ApplicationModule;
import de.htwk_leipzig.imn.eduwallchart.server.ServerEvents;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UserSettings;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;

import java.util.Timer;
import java.util.TimerTask;

/**
 * WallchartComponent konstruiert die Plantafel. Die Klasse holt sich die
 * Maschinen etc. und fertigt daraus die Plantafel an.
 *
 * @author Florian Klos
 *         Review Willi Reiche, 26.01.14
 */
@SuppressWarnings("serial")
public class WallchartComponent extends CustomLayout {


    NativeButton refreshButton;
    private Logger logger;
    private UserSettings userSettings;
    private WallchartViewComponent wallchartViewComponent;
    private Timer timer;

    @Inject
    public WallchartComponent(Logger logger, UserSettings userSettings, WallchartViewComponent wallchartViewComponent,
                              WallchartOptionsComponent wallchartOptionsComponent, @ApplicationModule.ServerEventBus EventBus eventBus) {
        super("wallchart");

        this.logger = logger;
        this.userSettings = userSettings;
        this.wallchartViewComponent = wallchartViewComponent;

        refreshButton = getRefreshButton();
        addComponent(refreshButton, "refresh");

        this.addComponent(wallchartOptionsComponent, "wallchart-options");
        this.addComponent(wallchartViewComponent, "wallchart-view");

        eventBus.register(this);

        if (userSettings.isAutoRefresh()) {
            timer = new Timer();
            timer.schedule(createResolutionUpdateTask(), LocalDateTime.now().plusDays(1).withTime(0, 0, 0, 0).toDate(), 1000 * 60 * 60 * 24);
        }
    }

    private TimerTask createResolutionUpdateTask() {
        return new TimerTask() {
            @Override
            public void run() {
                userSettings.setStart(userSettings.getStart().plusDays(1));
                if (userSettings.isCustomResolution()) {
                    userSettings.setEnd(userSettings.getEnd().plusDays(1));
                }
                getUI().push();
            }
        };
    }

    public void reload() {
        refreshButton.removeStyleName("new-data-available");
        refreshButton.setDescription("");
        wallchartViewComponent.refreshTimeLine();
    }

    /**
     * Methode, die vom Eventbus aufgerufen wird, sobald eine Liste von Maschinen auf den Eventbus geschickt wird.
     * Sie aktualisiert die Anzeige auf der Wallchartcomponent.
     *
     * @param dataChanged Event, dass Liste von Maschinen enthält
     */
    @Subscribe
    public void getMachineList(ServerEvents.DataChanged dataChanged) {
        boolean autorefresh = userSettings.isAutoRefresh();
        logger.debug("Refresh-Option: " + autorefresh);

        if (autorefresh) {
            wallchartViewComponent.refreshTimeLine();
        } else {
            modifyRefreshButton("new-data-available", "Klicken, um die Änderungen anzuzeigen.");
        }

        getUI().push();
        logger.debug(dataChanged.getMachineList().size() + " Machines pushed by Server.");
    }


    private NativeButton getRefreshButton() {
        NativeButton refreshButton = new NativeButton();
        refreshButton.setCaption("Aktualisieren");
        refreshButton.setIcon(new ThemeResource("img/icon_refresh.png"));
        refreshButton.setStyleName("button");
        refreshButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                reload();
            }
        });
        return refreshButton;
    }

    public void modifyRefreshButton(String styleName, String description) {
        refreshButton.addStyleName(styleName);
        refreshButton.setDescription(description);
    }
}
