package de.htwk_leipzig.imn.eduwallchart.ui.head;

import com.google.inject.Inject;
import com.vaadin.ui.*;
import de.htwk_leipzig.imn.eduwallchart.PathModule;
import de.htwk_leipzig.imn.eduwallchart.server.xslt.XSLT;
import de.htwk_leipzig.imn.eduwallchart.server.xslt.XSLTManager;
import de.htwk_leipzig.imn.eduwallchart.ui.util.ConfirmDialog;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UserSettings;

/**
 * Das Popup-Einstellungsmenü zum Speichern getätigter Einstellungen als Cookies
 *
 * @author Peter Otto
 */

public class SettingsMenu extends CustomLayout {

    VerticalLayout menuLayout = new VerticalLayout();
    private NativeButton saveButton;
    private CheckBox refreshCheckBox;
    private NativeSelect machineXSLComboBox, jobXSLComboBox;
    private UserSettings userSettings;

    @Inject
    public SettingsMenu(@PathModule.Machines XSLTManager machineXSLTManager, @PathModule.Jobs XSLTManager jobXSLTManager, UserSettings userSettings) {
        super("settingsMenu");

        this.userSettings = userSettings;

        Label settingsLabel = new Label("Einstellungen");
        settingsLabel.setStyleName("menu-chapter");
        menuLayout.addComponent(settingsLabel);

        machineXSLComboBox = createXSLComboBox(userSettings.getMachine(), machineXSLTManager);
        jobXSLComboBox = createXSLComboBox(userSettings.getJob(), jobXSLTManager);
        refreshCheckBox = new CheckBox("aktiviert", userSettings.isAutoRefresh());
        saveButton = createButton("Speichern", "save", createSaveListener());

        menuLayout.addComponent(createSettings());

        menuLayout.addComponent(saveButton);

        this.addComponent(menuLayout, "dialog");
    }

    private VerticalLayout createSettings() {
        VerticalLayout settings = new VerticalLayout();

        settings.addComponent(createSetting("Auto-Refresh", refreshCheckBox));
        settings.addComponent(createSetting("Maschinen-XSL", machineXSLComboBox));
        settings.addComponent(createSetting("Job-XSL", jobXSLComboBox));

        return settings;
    }

    private HorizontalLayout createSetting(String name, AbstractComponent component) {
        HorizontalLayout setting = new HorizontalLayout();
        setting.addStyleName("setting");

        Label settingName = new Label(name);
        settingName.addStyleName("setting-name");
        setting.addComponent(settingName);

        component.addStyleName("setting-input");
        setting.addComponent(component);

        return setting;
    }

    public void confirmAndSave() {
        if (!userSettings.isCookiesAccepted()) {
            final ConfirmDialog confirmDialog = new ConfirmDialog("Bitte bestätigen",
                    "Die geänderten Einstellungen werden als Cookies in Ihrem Browser gespeichert. Weiter?",
                    new CookiesOKListener(),
                    new CookiesCancelListener());
            getUI().addWindow(confirmDialog);
            confirmDialog.center();
        } else {
            saveSettings();
        }
    }

    private void saveSettings() {
        userSettings.setAutoRefresh(refreshCheckBox.getValue());
        userSettings.setMachine((XSLT) machineXSLComboBox.getValue());
        userSettings.setJob((XSLT) jobXSLComboBox.getValue());
    }

    private NativeSelect createXSLComboBox(XSLT defaultXSLT, XSLTManager xsltManager) {
        NativeSelect combobox = new NativeSelect();
        combobox.setWidth("100");

        combobox.setNullSelectionAllowed(false);

        combobox.addItem(defaultXSLT);
        combobox.select(defaultXSLT);

        for (XSLT xslt : xsltManager.getXSLTList()) {
            if (!xslt.getName().equals(defaultXSLT.getName())) {
                combobox.addItem(xslt);
            }
        }

        return combobox;
    }

    private NativeButton createButton(String name, String stylename, Button.ClickListener listener) {
        NativeButton button = new NativeButton(name);

        button.addClickListener(listener);
        button.addStyleName(stylename);
        return button;
    }

    private Button.ClickListener createSaveListener() {
        return new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                confirmAndSave();
            }
        };
    }

    private Panel createTablePanel(Object[] items) {
        Table table = new Table();
        table.addContainerProperty("Settings", Component.class, null);
        table.addContainerProperty("Values", Component.class, null);
        table.setColumnHeaderMode(Table.ColumnHeaderMode.HIDDEN);
        table.setColumnWidth("Settings", 113);
        table.setColumnWidth("Values", 121);
        table.addItem(items);
        table.setWidth("234");

        Panel tablePanel = new Panel();
        tablePanel.setWidth(100f, Unit.PERCENTAGE);
        tablePanel.setHeight("87");
        tablePanel.setContent(table);
        return tablePanel;
    }

    private static class CookiesCancelListener implements Button.ClickListener {
        @Override
        public void buttonClick(Button.ClickEvent clickEvent) {
        }
    }

    private class CookiesOKListener implements Button.ClickListener {
        @Override
        public void buttonClick(Button.ClickEvent clickEvent) {
            userSettings.setCookiesAccepted(true);
            saveSettings();
        }
    }
}
