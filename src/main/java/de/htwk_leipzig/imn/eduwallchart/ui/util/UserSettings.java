package de.htwk_leipzig.imn.eduwallchart.ui.util;

import com.google.common.base.Optional;
import com.google.common.eventbus.EventBus;
import de.htwk_leipzig.imn.eduwallchart.server.xslt.XSLT;
import de.htwk_leipzig.imn.eduwallchart.server.xslt.XSLTManager;
import org.joda.time.LocalDateTime;


/**
 * @author Florian Klos, 04.06.14.
 */
public class UserSettings {

    private UserData userData = new UserData();
    private CookieManager cookieManager;
    private EventBus sessionEventBus;

    public UserSettings(CookieManager cookieManager, XSLTManager machineXSLTManager, XSLTManager jobXSLTManager, EventBus sessionEventBus) {
        this.cookieManager = cookieManager;
        this.sessionEventBus = sessionEventBus;
        this.userData = createInitalData(cookieManager, machineXSLTManager, jobXSLTManager);
    }

    private static UserData createInitalData(CookieManager cookieManager, XSLTManager machineXSLTManager, XSLTManager jobXSLTManager) {
        UserData initialData = new UserData();
        initialData.cookiesAccepted = Boolean.parseBoolean(cookieManager.getCookie("CookiesAccepted").or("false"));
        initialData.timeLineResolution = TimeLineResolution.valueOf(cookieManager.getCookie("TimeLineResolution").or("WEEK"));
        initialData.customResolution = Boolean.parseBoolean(cookieManager.getCookie("CustomResolution").or("false"));
        initialData.start = LocalDateTime.parse(cookieManager.getCookie("StartDate").or(LocalDateTime.now().toString()));
        initialData.end = LocalDateTime.parse(cookieManager.getCookie("EndDate").or(initialData.timeLineResolution.plus(initialData.start).toString()));
        initialData.autoRefresh = Boolean.parseBoolean(cookieManager.getCookie("AutoRefresh").or("true"));
        initialData.machine = getXSLTbyCookie(cookieManager, "MachineXSL", machineXSLTManager);
        initialData.job = getXSLTbyCookie(cookieManager, "JobXSL", jobXSLTManager);
        return initialData;
    }

    private static XSLT getXSLTbyCookie(CookieManager cookieManager, String cookieName, XSLTManager manager) {
        Optional<String> xslName = cookieManager.getCookie(cookieName);
        if (xslName.isPresent()) {
            return manager.getXSLTbyName(xslName.get());
        }
        return manager.getDefault();
    }

    public boolean isCustomResolution() {
        return userData.customResolution;
    }

    public TimeLineResolution getTimeLineResolution() {
        return userData.timeLineResolution;
    }

    public void setTimeLineResolution(TimeLineResolution timeLineResolution) {
        userData.timeLineResolution = timeLineResolution;
        cookieManager.saveCookie("TimeLineResolution", timeLineResolution.name());
        userData.end = timeLineResolution.plus(userData.start);
        cookieManager.saveCookie("EndDate", userData.end.toString());
        userData.customResolution = false;
        cookieManager.saveCookie("CustomResolution", "false");
        sessionEventBus.post(new UIEvents.UserSettingsChangeEvent());
    }

    public boolean isAutoRefresh() {
        return userData.autoRefresh;
    }

    public void setAutoRefresh(boolean autoRefresh) {
        userData.autoRefresh = autoRefresh;
        cookieManager.saveCookie("AutoRefresh", String.valueOf(autoRefresh));
        sessionEventBus.post(new UIEvents.UserSettingsChangeEvent());
    }

    public boolean isCookiesAccepted() {
        return userData.cookiesAccepted;
    }

    public void setCookiesAccepted(boolean cookiesAccepted) {
        userData.cookiesAccepted = cookiesAccepted;
        cookieManager.saveCookie("CookiesAccepted", String.valueOf(cookiesAccepted));
        sessionEventBus.post(new UIEvents.UserSettingsChangeEvent());
    }

    public LocalDateTime getStart() {
        return userData.start;
    }

    public void setStart(LocalDateTime start) {
        if (userData.customResolution) {
            if (start.compareTo(userData.end) <= 0) {
                userData.start = start;
                cookieManager.saveCookie("StartDate", start.toString());
            }
        } else {
            userData.start = start;
            cookieManager.saveCookie("StartDate", start.toString());
            userData.end = userData.timeLineResolution.plus(userData.start);
            cookieManager.saveCookie("EndDate", userData.end.toString());
        }
        sessionEventBus.post(new UIEvents.UserSettingsChangeEvent());
    }

    public LocalDateTime getEnd() {
        return userData.end;
    }

    public void setEnd(LocalDateTime end) {
        if (end.compareTo(userData.start) >= 0) {
            userData.end = end;
            cookieManager.saveCookie("EndDate", end.toString());
            userData.customResolution = true;
            cookieManager.saveCookie("CustomResolution", String.valueOf("false"));
        }
        sessionEventBus.post(new UIEvents.UserSettingsChangeEvent());
    }

    public XSLT getMachine() {
        return userData.machine;
    }

    public void setMachine(XSLT machine) {
        userData.machine = machine;
        cookieManager.saveCookie("MachineXSL", machine.getName());
        sessionEventBus.post(new UIEvents.UserSettingsChangeEvent());
    }

    public XSLT getJob() {
        return userData.job;
    }

    public void setJob(XSLT job) {
        userData.job = job;
        cookieManager.saveCookie("JobXSL", job.getName());
        sessionEventBus.post(new UIEvents.UserSettingsChangeEvent());
    }

    public static class UserData {
        public boolean cookiesAccepted;
        public TimeLineResolution timeLineResolution;
        public boolean customResolution;
        public LocalDateTime start, end;
        public boolean autoRefresh;
        public XSLT machine, job;
    }
}
