package de.htwk_leipzig.imn.eduwallchart.ui.util;

import com.google.common.base.Optional;
import org.joda.time.DateTime;
import org.joda.time.Interval;

/**
 * @author Georg Felbinger, 02.07.14.
 */
public abstract class Intervals {
    public static Optional<Interval> getInterval(DateTime start, DateTime end) {
        Optional<Interval> intervalOptional;
        try {
            intervalOptional = Optional.fromNullable(new Interval(start, end));
        } catch (IllegalArgumentException e) {
            intervalOptional = Optional.absent();
        }
        return intervalOptional;
    }

    public static float inPercent(Interval interval, Interval base) {
        return ((float) interval.toDurationMillis() * 100) / base.toDurationMillis();
    }
}
