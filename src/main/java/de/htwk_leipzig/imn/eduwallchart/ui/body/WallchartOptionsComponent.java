package de.htwk_leipzig.imn.eduwallchart.ui.body;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.vaadin.data.Property;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PopupDateField;
import de.htwk_leipzig.imn.eduwallchart.ui.UIModule;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UIEvents;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UserSettings;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.Date;
import java.util.Locale;

/**
 * @author Georg Felbinger, 11.06.14.
 */
public class WallchartOptionsComponent extends CustomLayout {
    PopupDateField startDateField, endDateField;
    private Logger logger;
    private UserSettings userSettings;

    @Inject
    public WallchartOptionsComponent(Logger logger, UserSettings userSettings, @UIModule.SessionEventBus EventBus sessionEventBus) {
        super("wallchart-options");

        this.logger = logger;
        this.userSettings = userSettings;

        startDateField = createDateField(userSettings.getStart().toDate(), createStartDateFieldListener());
        endDateField = createDateField(userSettings.getEnd().toDate(), createEndDateFieldListener());

        addComponent(startDateField, "startdate");
        addComponent(new Label("oder"), "text");
        addComponent(endDateField, "enddate");
        addComponent(new ResolutionTabs(userSettings, sessionEventBus), "tabs");

        sessionEventBus.register(this);
    }

    private static void setDateFieldValue(final PopupDateField dateField, Date value) {
        Collection<Property.ValueChangeListener> listeners = (Collection<Property.ValueChangeListener>) dateField.getListeners(Property.ValueChangeEvent.class);

        for (Property.ValueChangeListener listener : listeners) {
            dateField.removeValueChangeListener(listener);
        }

        dateField.setValue(value);

        for (Property.ValueChangeListener listener : listeners) {
            dateField.addValueChangeListener(listener);
        }
    }

    private PopupDateField createDateField(Date value, Property.ValueChangeListener listener) {
        PopupDateField dateField = new PopupDateField();
        dateField.setValue(value);
        dateField.setLocale(Locale.GERMAN);
        dateField.setDateFormat("dd.MM.yyyy");
        dateField.setImmediate(true);
        dateField.setAssistiveText("");
        dateField.addValueChangeListener(listener);
        return dateField;
    }

    private Property.ValueChangeListener createStartDateFieldListener() {
        return new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                LocalDateTime startDate = new LocalDateTime(event.getProperty().getValue());
                userSettings.setStart(startDate);
            }
        };
    }

    private Property.ValueChangeListener createEndDateFieldListener() {
        return new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                LocalDateTime endDate = new LocalDateTime(event.getProperty().getValue());
                userSettings.setEnd(endDate);
            }
        };
    }

    @Subscribe
    public void setOptionsFromUserSettings(UIEvents.UserSettingsChangeEvent event) {
        setDateFieldValue(startDateField, userSettings.getStart().toDate());
        setDateFieldValue(endDateField, userSettings.getEnd().toDate());
    }
}
