package de.htwk_leipzig.imn.eduwallchart.ui.body;

import com.google.common.base.Optional;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.vaadin.annotations.JavaScript;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import de.htwk_leipzig.imn.eduwallchart.persistence.Job;
import de.htwk_leipzig.imn.eduwallchart.persistence.Machine;
import de.htwk_leipzig.imn.eduwallchart.persistence.MachineDao;
import de.htwk_leipzig.imn.eduwallchart.ui.UIModule;
import de.htwk_leipzig.imn.eduwallchart.ui.util.Intervals;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UIEvents;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UserSettings;
import org.joda.time.Interval;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Georg Felbinger 11.06.14.
 */
@JavaScript({"movemarker.js", "updateJobProgress.js"})
public class WallchartViewComponent extends CustomLayout {
    private MachineDao machineDao;
    private UserSettings userSettings;
    private VerticalLayout machineColumn = new VerticalLayout();
    private VerticalLayout jobColumn = new VerticalLayout();


    @Inject
    public WallchartViewComponent(MachineDao machineDao, Logger logger, UserSettings userSettings, @UIModule.SessionEventBus EventBus eventBus) {
        super("wallchart-view");
        this.machineDao = machineDao;
        this.userSettings = userSettings;
        this.addComponent(machineColumn, "machines");
        this.addComponent(jobColumn, "jobs");

        refreshTimeLine();

        eventBus.register(this);
    }

    void refreshTimeLine() {
        List<Machine> machines = machineDao.getAllMachines();
        final LocalDateTime startDate = userSettings.getStart();
        LocalDateTime endDate = userSettings.getEnd().hourOfDay().withMaximumValue()
                .minuteOfHour().withMaximumValue().secondOfMinute().withMaximumValue();
        LocalDateTime now = LocalDateTime.now();

        machineColumn.removeAllComponents();
        jobColumn.removeAllComponents();

        for (Machine machine : machines) {
            machineColumn.addComponent(new MachineComponent(machine, userSettings));
            Optional<Interval> timeLineIntervall = Intervals.getInterval(startDate.toDateTime(), endDate.toDateTime());
            if (timeLineIntervall.isPresent()) {
                jobColumn.addComponent(createJobRow(machine, timeLineIntervall.get()));
            } else {
                HorizontalLayout emptyRow = new HorizontalLayout();
                emptyRow.setStyleName("job-row");
                jobColumn.addComponent(emptyRow);
            }

        }
        if (machines.size() > 0 && now.compareTo(startDate) > 0 && now.compareTo(endDate) < 0) {
            jobColumn.addComponent(createMarker());
        }
    }

    private CustomLayout createMarker() {
        CustomLayout marker = new CustomLayout("marker");
        return marker;
    }

    private HorizontalLayout createJobRow(Machine machine, Interval timelineInterval) {
        HorizontalLayout jobrow = new HorizontalLayout();
        LocalDateTime lastDate = timelineInterval.getStart().toLocalDateTime();
        final List<Job> jobList = machine.getJobs();
        Collections.sort(jobList, new StartDateComparator());
        jobrow.setSizeFull();

        for (Job job : jobList) {
            if (job.getStartTime().isPresent() && job.getEndTime().isPresent()) {
                Optional<Interval> jobInterval = Intervals.getInterval(job.getStartTime().get(), job.getEndTime().get());
                if (jobInterval.isPresent()) {
                    Optional<Interval> jobTimeLineInterval = Optional.fromNullable(timelineInterval.overlap(jobInterval.get()));
                    if (jobTimeLineInterval.isPresent()) {
                        Optional<Interval> spacerInterval = Intervals.getInterval(lastDate.toDateTime(), jobTimeLineInterval.get().getStart());
                        if (spacerInterval.isPresent()) {
                            buildAndAddSpacer(spacerInterval.get(), timelineInterval, jobrow);
                        }
                        buildAndAddJobComponent(job, jobTimeLineInterval.get(), timelineInterval, jobrow);
                        lastDate = jobTimeLineInterval.get().getEnd().toLocalDateTime();
                    }
                }
            }
        }
        Optional<Interval> spacerInterval = Intervals.getInterval(lastDate.toDateTime(), timelineInterval.getEnd());
        if (spacerInterval.isPresent()) {
            buildAndAddSpacer(spacerInterval.get(), timelineInterval, jobrow);
        }
        jobrow.setStyleName("job-row");

        return jobrow;
    }

    private void buildAndAddJobComponent(Job job, Interval jobTimeLineInterval, Interval timelineInterval, HorizontalLayout jobrow) {
        float jobPercent = Intervals.inPercent(jobTimeLineInterval, timelineInterval);
        JobComponent jobComponent = new JobComponent(job, userSettings);
        jobComponent.setStyleName(job.getStatus().name().toLowerCase());
        jobrow.addComponent(jobComponent);
        jobrow.setExpandRatio(jobComponent, jobPercent);
    }

    private void buildAndAddSpacer(Interval spacerInterval, Interval timelineInterval, HorizontalLayout jobrow) {
        float spacerPercent = Intervals.inPercent(spacerInterval, timelineInterval);
        Label spacer = new Label("");
        spacer.setHeight("1px");
        jobrow.addComponent(spacer);
        jobrow.setExpandRatio(spacer, spacerPercent);
    }

    @Subscribe
    public void setOptionsFromUserSettings(UIEvents.UserSettingsChangeEvent event) {
        refreshTimeLine();
    }

    public class StartDateComparator implements Comparator<Job> {
        @Override
        public int compare(Job o1, Job o2) {
            if (o1.getStartTime().isPresent() && o2.getStartTime().isPresent()) {
                return o1.getStartTime().get().compareTo(o2.getStartTime().get());
            } else {
                return 0;
            }
        }
    }
}
