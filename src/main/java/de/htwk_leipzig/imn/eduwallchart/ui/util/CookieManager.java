package de.htwk_leipzig.imn.eduwallchart.ui.util;

import com.google.common.base.Optional;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.JavaScript;
import org.slf4j.Logger;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.Map;

/**
 * Klasse zum Verwalten der Cookies auf den Clients
 *
 * @author Georg Felbinger
 *         Review: Willi Reiche am 04.06.
 */
public class CookieManager {

    private Map<String, String> cookies = new HashMap<>();
    private Logger logger;
    private JavaScript javaScript;

    /**
     * Konstruktor, initialisiert alle im Request verfügbaren Cookies
     *
     * @param javaScript JS-Kontext, mit dem neue Cookies gespeichert werden können
     * @param logger     Ein Logger
     */
    public CookieManager(JavaScript javaScript, VaadinRequest vaadinRequest, Logger logger) {
        this.logger = logger;
        this.javaScript = javaScript;

        Cookie[] cookies = vaadinRequest.getCookies();
        for (Cookie cookie : cookies) {
            logger.debug("Loading Cookie '" + cookie.getName() + "' with Value '" + cookie.getValue() + "'");
            this.cookies.put(cookie.getName(), cookie.getValue());
        }
    }

    /**
     * Methode zum Speichern neuer Cookies
     *
     * @param name  Name des Cookies
     * @param value Wert des Cookies
     */
    public void saveCookie(String name, String value) {
        if ((name.equals("CookiesAccepted")) && (value.equals(String.valueOf(true)))) {
            logger.debug("Saving Cookie '" + name + "' with Value '" + value + "'");
            javaScript.execute("document.cookie=\"" + name + "=" + value + "\";");
            cookies.put(name, value);
        } else {
            Optional<String> cookiesAccepted = Optional.fromNullable(cookies.get("CookiesAccepted"));
            if (cookiesAccepted.or("false").equals(String.valueOf(true))) {
                logger.debug("Saving Cookie '" + name + "' with Value '" + value + "'");
                javaScript.execute("document.cookie=\"" + name + "=" + value + "\";");
                cookies.put(name, value);
            }
        }
    }

    /**
     * Methode zum Abrufen von Cookies
     *
     * @param name Name des Cookies
     * @return Wert des Cookies wenn dieser vorhanden ist, sonst absent
     */
    public Optional<String> getCookie(String name) {
        logger.debug("Read cookie '" + name + "': " + cookies.get(name));
        return Optional.fromNullable(cookies.get(name));
    }
}
