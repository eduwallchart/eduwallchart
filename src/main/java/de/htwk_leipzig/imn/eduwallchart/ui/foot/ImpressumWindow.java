package de.htwk_leipzig.imn.eduwallchart.ui.foot;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * ImpressumWindow konstruiert das Impressumsfenster
 *
 * @author Florian Klos
 *         <p/>
 *         Review: Willi Reiche, 26.01.14
 */

@SuppressWarnings("serial")
public class ImpressumWindow extends Window {

    private Label impressum;
    private boolean isVisible = false;

    public ImpressumWindow() {
        center();
        impressum = new Label("Beim Lesen des Impressums ist ein Fehler aufgetreten.", ContentMode.HTML);
        isVisible = false;
        addStyleName("impressum");
        setContent(getContentLayout());
    }

    private Layout getContentLayout() {
        VerticalLayout content = new VerticalLayout();
        content.addComponent(impressum);
        Button close = new Button("Impressum schließen");
        close.addClickListener(new ClickListener() {
            public void buttonClick(ClickEvent event) {
                close();
                setVisibility(false);
            }
        });
        content.addComponent(close);
        return content;
    }

    /**
     * Setzt die Impressumsdatei und versucht diese auszulesen.
     * Wenn Auslesen erfolgreich war, wird der Text dem Impressumsfenster hinzugefügt.
     * @param impressumFile Impressums-File
     */
    public void setImpressumFile(File impressumFile) {
        try {
            String impressumText = FileUtils.readFileToString(impressumFile, "UTF-8");
            impressum.setValue(impressumText);
        } catch (IOException ignored) {
        }
    }

    /**
     * Prüft ob das Subwindow geöffnet ist, damit es nicht mehr
     * als ein mal geöffnet wird.
     * @return true wenn schon geöffnet
     */
    public boolean isVisible() {
        return isVisible;
    }

    /**
     * Setzt die Sichtbarkeit des Fensters.
     * True wenn offen, False wenn geschlossen.
     * @param visible true wenn offen
     */
    public void setVisibility(boolean visible) {
        isVisible = visible;
    }
}
