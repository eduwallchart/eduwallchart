package de.htwk_leipzig.imn.eduwallchart.ui.util;

import com.google.common.base.Function;
import org.joda.time.LocalDateTime;


/**
 * @author Georg Felbinger, 05.06.14.
 */
public enum TimeLineResolution {
    DAY(new Function<LocalDateTime, LocalDateTime>() {
        @Override
        public LocalDateTime apply(LocalDateTime localDateTime) {
            return localDateTime.plusDays(1).minusSeconds(1);
        }
    }), WEEK(new Function<LocalDateTime, LocalDateTime>() {
        @Override
        public LocalDateTime apply(LocalDateTime localDateTime) {
            return localDateTime.plusWeeks(1).minusSeconds(1);
        }
    }), MONTH(new Function<LocalDateTime, LocalDateTime>() {
        @Override
        public LocalDateTime apply(LocalDateTime localDateTime) {
            return localDateTime.plusMonths(1).minusSeconds(1);
        }
    });

    private final Function<LocalDateTime, LocalDateTime> calcFunction;

    TimeLineResolution(Function<LocalDateTime, LocalDateTime> calcFunction) {
        this.calcFunction = calcFunction;
    }

    public LocalDateTime plus(LocalDateTime start) {
        return calcFunction.apply(start);
    }
}
