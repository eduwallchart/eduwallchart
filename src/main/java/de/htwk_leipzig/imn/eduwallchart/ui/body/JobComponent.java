package de.htwk_leipzig.imn.eduwallchart.ui.body;

import com.google.common.eventbus.Subscribe;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import de.htwk_leipzig.imn.eduwallchart.persistence.Job;
import de.htwk_leipzig.imn.eduwallchart.server.XSLTransformer;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UIEvents;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UserSettings;

import static de.htwk_leipzig.imn.eduwallchart.ui.EduWallChartUI.getCurrentSessionEventBus;

/**
 * JobComponent generiert ein Jobpanel für die Anzeige in der UI
 * aus einem übergebenen Jobobjekt
 *
 * @author Georg Felbinger
 *         Reviewer: Peter Nancke
 */

@SuppressWarnings("serial")
public class JobComponent extends CustomLayout {

    private static final XSLTransformer XSL_TRANSFORMER = new XSLTransformer();
    private final Job job;
    private final UserSettings userSettings;

    /**
     * MachineComponent erstellt ein Panel, welches alle Maschineninformationen in
     * verschiedenen Labels wiedergibt
     *
     * @param job          Ein Jobobjekt
     * @param userSettings Das UserSettings-Objekt
     */
    public JobComponent(final Job job, UserSettings userSettings) {
        super("job");
        this.job = job;
        this.userSettings = userSettings;
        getCurrentSessionEventBus().register(this);
        String jobID = "job-" + job.getId().replaceAll(" ", "-");
        this.setId(jobID);
        this.addComponent(new Label(job.getStartTime().get().toString()), "startdate");
        this.addComponent(new Label(job.getEndTime().get().toString()), "enddate");
        this.addComponent(new Label(job.getTimeProgress().get() + "%"), "progress");
        this.addComponent(new Label(job.getId()), "id");
        this.addComponent(new Label(job.getName()), "name");
        this.addComponent(new Label(job.getStatus().toString()), "status");
        loadTooltip();
    }

    private void loadTooltip() {
        Label tooltip = new Label(XSL_TRANSFORMER.getJobTooltip(job, userSettings.getJob().getPath()), ContentMode.HTML);
        this.addComponent(tooltip, "tooltip");
    }

    @Subscribe
    public void reloadTooltip(UIEvents.UserSettingsChangeEvent settingsChangeEvent) {
        loadTooltip();
    }

}