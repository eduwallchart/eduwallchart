package de.htwk_leipzig.imn.eduwallchart.ui.foot;

import com.vaadin.ui.Button;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.UI;

import java.io.File;
import java.nio.file.Path;

/**
 * @author Florian Klos
 */
public class FooterComponent extends CustomLayout {

    private ImpressumWindow impressumSub;

    public FooterComponent() {
        super("footer");
        this.setStyleName("site-footer");
        impressumSub = new ImpressumWindow();
        addImpressum();
    }

    private void addImpressum() {
        final Button impressum = new Button("Impressum");
        impressum.setStyleName("link");
        impressum.addClickListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                if (!impressumSub.isVisible()) {
                    UI.getCurrent().addWindow(impressumSub);
                    impressumSub.setVisibility(true);
                }
            }
        });
        this.addComponent(impressum, "right");
    }

    /**
     * Setzt den Pfad zum Impressumsordner.
     * Die Methode sucht dann nach einer Datei "impressum.html" um
     * diese dem Impressumsfenster zur Verfügung zu stellen.
     * @param path Pfad zum impressum-Ordner
     */
    public void setImpressumPath(final Path path) {
        File folder = path.toFile();
        for (final File file : folder.listFiles()) {
            if (file.getName().equals("impressum.html")) {
                impressumSub.setImpressumFile(file);
            }
        }
    }
}
