package de.htwk_leipzig.imn.eduwallchart.ui;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.UI;
import de.htwk_leipzig.imn.eduwallchart.PathModule;
import de.htwk_leipzig.imn.eduwallchart.server.xslt.XSLTManager;
import de.htwk_leipzig.imn.eduwallchart.ui.body.WallchartComponent;
import de.htwk_leipzig.imn.eduwallchart.ui.foot.FooterComponent;
import de.htwk_leipzig.imn.eduwallchart.ui.head.HeaderComponent;
import de.htwk_leipzig.imn.eduwallchart.ui.util.CookieManager;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UserSettings;
import org.slf4j.Logger;

import java.nio.file.Path;

import static de.htwk_leipzig.imn.eduwallchart.ApplicationFilter.getInjector;

/**
 * EduWallChartUI ist der Ausgangspunkt der UI. Von hier aus wird die Anwendung
 * gestartet.
 * Die Klasse implementiert Broadcaster.MachineBroadcastListener zum Empfang von Updates vom Server
 *
 * @author Florian Klos
 *         <p/>
 *         Review: Willi Reiche, 26.01.14
 */
@SuppressWarnings("serial")
@Push(PushMode.MANUAL)
@Theme("ewchart")
@JavaScript({"http://code.jquery.com/jquery-2.1.1.min.js",
        "http://cdn.jsdelivr.net/qtip2/2.2.0/jquery.qtip.min.js",
        "enableqtip.js"})
public class EduWallChartUI extends UI {

    private static CookieManager cookieManager;
    private static UserSettings userSettings;
    private static EventBus sessionEventBus;

    @Inject @PathModule.Machines XSLTManager machineXSLTManager;
    @Inject @PathModule.Jobs XSLTManager jobXSLTManager;
    @Inject @PathModule.Impressum Path impressumPath;

    public static CookieManager getCurrentCookieManager() {
        return cookieManager;
    }

    public static UserSettings getCurrentUserSettings() {
        return userSettings;
    }

    public static EventBus getCurrentSessionEventBus() {
        return sessionEventBus;
    }

    @Override
    protected void init(VaadinRequest request) {
        Injector injector = getInjector();
        Logger logger = injector.getInstance(Logger.class);

        cookieManager = new CookieManager(this.getPage().getJavaScript(), VaadinService.getCurrentRequest(), logger);
        sessionEventBus = new EventBus();
        userSettings = new UserSettings(cookieManager, machineXSLTManager, jobXSLTManager, sessionEventBus);

        HeaderComponent headerComponent = injector.getInstance(HeaderComponent.class);
        WallchartComponent wallchartComponent = injector.getInstance(WallchartComponent.class);
        FooterComponent footerComponent = injector.getInstance(FooterComponent.class);
        footerComponent.setImpressumPath(impressumPath);

        final CustomLayout layout = new CustomLayout("main");
        layout.addComponent(headerComponent, "header");
        layout.addComponent(wallchartComponent, "content");
        layout.addComponent(footerComponent, "footer");
        setContent(layout);
    }

}
