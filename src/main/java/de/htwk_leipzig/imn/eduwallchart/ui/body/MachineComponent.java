package de.htwk_leipzig.imn.eduwallchart.ui.body;

import com.google.common.eventbus.Subscribe;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import de.htwk_leipzig.imn.eduwallchart.persistence.Machine;
import de.htwk_leipzig.imn.eduwallchart.server.XSLTransformer;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UIEvents;
import de.htwk_leipzig.imn.eduwallchart.ui.util.UserSettings;

import static de.htwk_leipzig.imn.eduwallchart.ui.EduWallChartUI.getCurrentSessionEventBus;

/**
 * MachineComponent generiert ein Maschinenpanel für die Anzeige in der UI
 * aus einem übergebenen Maschinenobjekt
 *
 * @author Willi Reiche
 *         Reviewer: Peter Nancke
 */

@SuppressWarnings("serial")
public class MachineComponent extends CustomLayout {

    private static final XSLTransformer XSL_TRANSFORMER = new XSLTransformer();
    private final Machine machine;
    private final UserSettings userSettings;

    /**
     * MachineComponent erstellt ein Panel, welches alle Maschineninformationen in
     * verschiedenen Labels wiedergibt
     *
     * @param machine      Ein Maschinenobjekt
     * @param userSettings UserSettings
     */
    public MachineComponent(final Machine machine, UserSettings userSettings) {
        super("machine");
        this.machine = machine;
        this.userSettings = userSettings;
        getCurrentSessionEventBus().register(this);
        this.setId("machine-" + machine.getId().replaceAll(" ", "-"));
        this.addComponent(new Label(machine.getId()), "id");
        this.addComponent(new Label(machine.getName()), "name");
        this.addComponent(new Label(machine.getMachineStatus().toString()), "status");
        loadTooltip();
    }

    private void loadTooltip() {
        Label tooltip = new Label(XSL_TRANSFORMER.getMachineTooltip(machine, userSettings.getMachine().getPath()), ContentMode.HTML);
        this.addComponent(tooltip, "tooltip");
    }

    @Subscribe
    public void reloadTooltip(UIEvents.UserSettingsChangeEvent settingsChangeEvent) {
        loadTooltip();
    }
}