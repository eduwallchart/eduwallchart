package de.htwk_leipzig.imn.eduwallchart.ui.util;

import com.vaadin.ui.*;

/**
 * Dialog zum Akzeptieren von Cookies
 *
 * @author Georg Felbinger, 04.06.2014
 *         Review Willi Reiche
 */
public class ConfirmDialog extends Window {

    private CustomLayout layout;
    private NativeButton ok = new NativeButton("Ok");
    private NativeButton cancel = new NativeButton("Abbrechen");

    private Button.ClickListener closeListener = new Button.ClickListener() {
        @Override
        public void buttonClick(Button.ClickEvent clickEvent) {
            close();
        }
    };


    public ConfirmDialog(String title, String text, Button.ClickListener onOk, Button.ClickListener onCancel) {
        layout = new CustomLayout("popupWindow");

        HorizontalLayout header = new HorizontalLayout();
        header.addComponent(new Label(title));
        layout.addComponent(header, "header");

        VerticalLayout content = new VerticalLayout();
        content.addComponent(new Label(text));

        HorizontalLayout buttons = new HorizontalLayout();
        ok.addStyleName("popup-button");
        ok.addClickListener(onOk);
        ok.addClickListener(closeListener);
        cancel.addStyleName("popup-button");
        cancel.addClickListener(onCancel);
        cancel.addClickListener(closeListener);
        buttons.addComponents(ok, cancel, new Label());
        content.addComponent(buttons);

        layout.addComponent(content, "content");

        this.setContent(layout);
        this.setId("tooltip-confirmDialog");
    }
}
