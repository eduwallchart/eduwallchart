package de.htwk_leipzig.imn.eduwallchart.ui.head;

import com.google.inject.Inject;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Link;

/**
 * HeaderComponent konstruiert den Header der Seite. Dazu gehören das Logo,
 * sowie ein Link zum Impressum.
 *
 * @author Florian Klos
 *         Review: Willi Reiche, 26.01.14
 */
@SuppressWarnings("serial")
public class HeaderComponent extends CustomLayout {

    @Inject
    public HeaderComponent(SettingsMenu settingsMenu) {
        super("header");
        this.setStyleName("site-header");
        this.addComponent(settingsMenu, "options");
        Link link = new Link(null, new ThemeResource("tools/readme.html"));
        link.setIcon(new ThemeResource("img/icon_question_mark.png"));
        link.setTargetName("_blank");
        this.addComponent(link, "help");
    }

}
