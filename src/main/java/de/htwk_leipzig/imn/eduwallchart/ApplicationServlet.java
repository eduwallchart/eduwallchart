package de.htwk_leipzig.imn.eduwallchart;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.vaadin.server.*;

import javax.servlet.ServletException;
import java.util.Properties;

/**
 * Das ApplicationServlet stellt das VaadinServlet dar und hat einen Listener, der
 * auf die Initialisierung der Session und des Servlets wartet.
 *
 * @author Willi Reiche
 */
@Singleton
public class ApplicationServlet extends VaadinServlet implements SessionInitListener {

    @Inject ApplicationProvider provider;

    @Override
    protected DeploymentConfiguration createDeploymentConfiguration(Properties initParameters) {
        initParameters.setProperty(SERVLET_PARAMETER_PRODUCTION_MODE, "true");
        return super.createDeploymentConfiguration(initParameters);
    }

    @Override
    protected void servletInitialized() throws ServletException {
        getService().addSessionInitListener(this);
    }

    @Override
    public void sessionInit(SessionInitEvent event) throws ServiceException {
        event.getSession().addUIProvider(provider);
    }
}
