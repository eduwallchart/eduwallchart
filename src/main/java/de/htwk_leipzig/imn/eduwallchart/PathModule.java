package de.htwk_leipzig.imn.eduwallchart;

import com.google.inject.*;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.nio.file.Files.createDirectory;

/**
 * Das PathModule stellt Dependencies für den Homepfad und dessen Unterordner bereit.
 * Resultierende Ordnerstruktur soll folgendermaßen aussehen:
 * <p/>
 * ~home directory
 * |
 * +---eduwallchart
 *      |
 *      +---hotfolder
 *      |
 *      +---jdf
 *          |
 *          +---success
 *          |
 *          +---failed
 *      |
 *      +---logs
 *      |
 *      +---templates
 *          |
 *          +---jobs
 *          |
 *          +---machines
 *      |
 *      +---impressum
 *      |
 *      +---weiteres Unterverzeichnis A
 *      |
 *      +---weiteres Unterverzeichnis B
 *      |
 *      +--- ...
 * <p/>
 * @author Willi Reiche on 06.05.14.
 */


public class PathModule extends AbstractModule {

    @Override
    protected void configure() {

    }

    @Provides
    @Home
    @Singleton
    private Path provideHomePath() throws IOException {
        Path home = Paths.get(System.getProperty("user.home")).resolve("eduwallchart");
        if (Files.notExists(home)) {
            createDirectory(home);
        }
        return home;
    }

    @Provides
    @JdfHotfolder
    @Inject
    @Singleton
    private Path provideJdfHotfolderPath(@Home Path home) throws IOException {
        Path hotfolder = home.resolve("hotfolder");
        if (Files.notExists(hotfolder)) {
            Files.createDirectory(hotfolder);
        }
        return hotfolder;
    }

    @Provides
    @JdfSuccessPath
    @Inject
    @Singleton
    private Path provideJdfSuccessPath(@Home Path home) throws IOException {
        Path jdfSuccessFolder = home.resolve("jdf").resolve("success");
        if (Files.notExists(jdfSuccessFolder)) {
            Files.createDirectories(jdfSuccessFolder);
        }
        return jdfSuccessFolder;
    }

    @Provides
    @JdfFailPath
    @Inject
    @Singleton
    private Path provideJdfFailPath(@Home Path home) throws IOException {
        Path jdfFailedFolder = home.resolve("jdf").resolve("failed");
        if (Files.notExists(jdfFailedFolder)) {
            Files.createDirectories(jdfFailedFolder);
        }
        return jdfFailedFolder;
    }

    @Provides
    @Logs
    @Inject
    @Singleton
    private Path provideLoggingPath(@Home Path home) throws IOException {
        Path logging = home.resolve("logs");
        if (Files.notExists(logging)) {
            Files.createDirectory(logging);
        }
        return logging;
    }

    @Provides
    @Templates
    @Inject
    @Singleton
    private Path provideTemplatesPath(@Home Path home) throws IOException {
        Path templates = home.resolve("templates");
        if (Files.notExists(templates)) {
            Files.createDirectories(templates);
        }
        return templates;
    }


    @Provides
    @Machines
    @Inject
    @Singleton
    private Path provideMachinesTemplatesPath(@Home Path home) throws IOException {
        Path machines = home.resolve("templates").resolve("machines");
        if (Files.notExists(machines)) {
            Files.createDirectories(machines);
        }
        return machines;
    }

    @Provides
    @Jobs
    @Inject
    @Singleton
    private Path provideJobsTemplatesPath(@Home Path home) throws IOException {
        Path jobs = home.resolve("templates").resolve("jobs");
        if (Files.notExists(jobs)) {
            Files.createDirectories(jobs);
        }
        return jobs;
    }

    @Provides
    @Impressum
    @Inject
    @Singleton
    private Path provideImpressumPath(@Home Path home) throws IOException {
        Path impressum = home.resolve("impressum");
        if (Files.notExists(impressum)) {
            Files.createDirectory(impressum);
        }
        return impressum;
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface Home {
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface JdfHotfolder {
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface JdfSuccessPath {
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface JdfFailPath {
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface Logs {
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface Templates {
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface Machines {
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface Jobs {
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface Impressum {
    }
}

