package de.htwk_leipzig.imn.eduwallchart;

import com.google.common.collect.ImmutableMap;
import com.google.common.eventbus.EventBus;
import com.google.inject.BindingAnnotation;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.servlet.ServletModule;
import com.vaadin.ui.UI;
import de.htwk_leipzig.imn.eduwallchart.persistence.PersistenceModule;
import de.htwk_leipzig.imn.eduwallchart.server.ServerModule;
import de.htwk_leipzig.imn.eduwallchart.ui.EduWallChartUI;
import de.htwk_leipzig.imn.eduwallchart.ui.UIModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Das ServletModule konfiguriert das ApplicationsServlet, sodass alle Requests verarbeitet werden.
 * Außerdem werden die Packetspezifischen Module geladen.
 *
 * @author Willi Reiche
 */
public class ApplicationModule extends ServletModule {

    @Override
    protected void configureServlets() {
        serve("/*").with(ApplicationServlet.class, ImmutableMap.of("productionMode", "false"));

        installModules();
    }

    private void installModules() {
        install(new PersistenceModule());
        install(new ServerModule());
        install(new UIModule());
        install(new PathModule());

        install(new PostConstructModule());
    }


    @Provides
    @Singleton
    private Logger provideLogger() {
        return LoggerFactory.getLogger("ewchart");
    }

    @Provides
    @Singleton
    private Class<? extends UI> provideUIClass() {
        return EduWallChartUI.class;
    }


    @Provides
    @Singleton
    private ExecutorService provideExecutorService() {
        return Executors.newFixedThreadPool(2);
    }

    @Provides
    @ServerEventBus
    @Singleton
    private EventBus provideEventBus() {
        return new EventBus();
    }

    @BindingAnnotation
    @Target({FIELD, PARAMETER, METHOD})
    @Retention(RUNTIME)
    public static @interface ServerEventBus {
    }
}
