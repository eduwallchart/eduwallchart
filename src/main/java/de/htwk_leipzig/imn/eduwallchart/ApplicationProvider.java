package de.htwk_leipzig.imn.eduwallchart;

import com.google.inject.Inject;
import com.vaadin.server.UIClassSelectionEvent;
import com.vaadin.server.UICreateEvent;
import com.vaadin.server.UIProvider;
import com.vaadin.ui.UI;

/**
 * Der ApplicationProvider erstellt die Vaadin-UI Instanzen.
 *
 * @author Willi Reiche
 */
public class ApplicationProvider extends UIProvider {

    @Inject Class<? extends UI> uiClass;

    @Override
    public UI createInstance(UICreateEvent event) {
        return ApplicationFilter.getInjector().getProvider(uiClass).get();
    }

    @Override
    public Class<? extends UI> getUIClass(UIClassSelectionEvent event) {
        return uiClass;
    }
}