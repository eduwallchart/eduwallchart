package de.htwk_leipzig.imn.eduwallchart;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.servlet.GuiceFilter;
import org.slf4j.Logger;
import javax.servlet.*;
import java.io.IOException;

import static de.htwk_leipzig.imn.eduwallchart.ApplicationModule.ServerEventBus;

/**
 * Der Applikationsfilter filtert ServletRequests und leitet sie an Vaadin weiter.
 *
 * @author Willi Reiche
 */
public class ApplicationFilter extends GuiceFilter {

    private static Injector INJECTOR = Guice.createInjector(new ApplicationModule());

    private final EventBus eventBus = INJECTOR.getInstance(Key.get(EventBus.class, ServerEventBus.class));
    private final Logger logger = INJECTOR.getInstance(Logger.class);

    public static Injector getInjector() {
        return INJECTOR;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        super.doFilter(request, response, chain);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.debug("Created injector with " + INJECTOR.getAllBindings().size() + " bindings.");
        eventBus.register(this);
        super.init(filterConfig);
    }

    @Override
    public void destroy() {
        eventBus.post(new ApplicationEvents.Shutdown());
        super.destroy();
    }

    @Subscribe
    public void recordDeadEvent(DeadEvent deadEvent) {
        logger.info("Dead event: " + deadEvent.getEvent() + " registered from " + deadEvent.getSource());
    }
}
