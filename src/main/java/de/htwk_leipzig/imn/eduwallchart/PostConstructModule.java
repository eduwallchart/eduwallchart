package de.htwk_leipzig.imn.eduwallchart;

import com.google.common.base.Predicate;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.Matchers;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;

import static com.google.common.base.Optional.fromNullable;
import static com.google.common.collect.Iterables.filter;
import static java.lang.String.format;
import static java.util.Arrays.asList;

/**
 * Das PostConstructModule erm&ouml;glicht es nach dem dependency-injection gest&uuml;tzten Konstruieren
 * eines Objekts weitere Aktionen zur Initialisierung auszuf&uuml;hren.
 *
 * @author Willi Reiche
 * @see //docs.oracle.com/javaee/7/tutorial/doc/cdi-basic014.htm
 */
public class PostConstructModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bindListener(Matchers.any(), new PostConstructTypeLiteral());
    }

    private class PostConstructTypeLiteral implements TypeListener {
        @Override
        public <I> void hear(TypeLiteral<I> literal, TypeEncounter<I> encounter) {
            encounter.register(new PostConstructListener<I>());
        }
    }

    private class PostConstructListener<I> implements InjectionListener<I> {
        @Override
        public void afterInjection(I injectee) {
            for (Method method : filter(asList(injectee.getClass().getMethods()), new PostContructPredicate())) {
                try {
                    method.invoke(injectee);
                } catch (Exception e) {
                    throw new RuntimeException(format("Unable to invoke @PostConstruct %s", method), e);
                }
            }
        }
    }

    private class PostContructPredicate implements Predicate<Method> {
        @Override
        public boolean apply(Method method) {
            return fromNullable(method.getAnnotation(PostConstruct.class)).isPresent();
        }
    }
}