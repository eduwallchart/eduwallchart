<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html"/>
    <xsl:template name="Machine-Special" match="machine">
        <xsl:if test="id != ''">
            <div class="property">
                <div class="name">Identifier:</div>
                <div class="value">
                    <xsl:value-of select="id"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="machineStatus != ''">
            <div class="property">
                <div class="name">Status:</div>
                <div class="value {machineStatus}">
                    <xsl:value-of select="machineStatus"/>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>