<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html"/>
    <xsl:template name="Machine-Default" match="machine">
        <xsl:if test="id != ''">
            <div class="property">
                <div class="name">ID:</div>
                <div class="value">
                    <xsl:value-of select="id"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="name != ''">
            <div class="property">
                <div class="name">Name:</div>
                <div class="value">
                    <xsl:value-of select="name"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="machineStatus != ''">
            <div class="property">
                <div class="name">Status:</div>
                <div class="value">
                    <xsl:value-of select="machineStatus"/>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    <xsl:template name="Job-Default" match="job">
        <xsl:if test="id != ''">
            <div class="property">
                <div class="name">ID:</div>
                <div class="value">
                    <xsl:value-of select="id"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="name != ''">
            <div class="property">
                <div class="name">Name:</div>
                <div class="value">
                    <xsl:value-of select="name"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="startTime != ''">
            <div class="property">
                <div class="name">Status:</div>
                <div class="value">
                    <xsl:value-of select="startTime"/>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="endTime != ''">
            <div class="property">
                <div class="name">Status:</div>
                <div class="value">
                    <xsl:value-of select="endTime"/>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>