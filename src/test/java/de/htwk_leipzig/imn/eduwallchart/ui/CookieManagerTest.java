package de.htwk_leipzig.imn.eduwallchart.ui;

import com.google.common.base.Optional;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.JavaScript;
import de.htwk_leipzig.imn.eduwallchart.ui.util.CookieManager;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import javax.servlet.http.Cookie;

/**
 * @author Peter Nancke
 */
public class CookieManagerTest {
    @Mock JavaScript javaScript;
    @Mock VaadinRequest vaadinRequest;
    @Mock Logger logger;

    String existingCookieName = "existingCookie";
    String existingCookieValue = "0";
    CookieManager cookieManager;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);


        Mockito.doReturn(new Cookie[]{new Cookie(existingCookieName, existingCookieValue)}).when(vaadinRequest).getCookies();
        cookieManager = new CookieManager(javaScript, vaadinRequest, logger);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testSaveCookie() throws Exception {
        String newCookieName = "newCookie";
        String newCookieValue = "1";

        cookieManager.saveCookie(newCookieName, newCookieValue);
        TestCase.assertEquals(Optional.of(existingCookieValue), cookieManager.getCookie(existingCookieName));
        TestCase.assertEquals(Optional.absent(), cookieManager.getCookie(newCookieName));

        cookieManager.saveCookie("CookiesAccepted", String.valueOf(true));
        TestCase.assertEquals(Optional.of(String.valueOf(true)), cookieManager.getCookie("CookiesAccepted"));

        cookieManager.saveCookie(newCookieName, newCookieValue);
        TestCase.assertEquals(Optional.of(newCookieValue), cookieManager.getCookie(newCookieName));
    }
}