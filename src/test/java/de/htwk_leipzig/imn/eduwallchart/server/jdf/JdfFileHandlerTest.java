package de.htwk_leipzig.imn.eduwallchart.server.jdf;

import de.htwk_leipzig.imn.eduwallchart.TestPaths;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.HandleFileException;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;

public class JdfFileHandlerTest {
    @Mock Logger logger;
    @Mock JdfFileParser jdfFileParser;
    JdfFileHandler jdfFileHandler;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        jdfFileHandler = new JdfFileHandler();
        jdfFileHandler.jdfFileParser = jdfFileParser;
        jdfFileHandler.logger = logger;
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testHandleFile() throws Exception {
        Path jdfFile = TestPaths.hotfolder.resolve("test.jdf");
        jdfFileHandler.jdfSuccessPath = TestPaths.jdfSuccess;
        jdfFileHandler.jdfFailPath = TestPaths.jdfFailed;
        jdfFileHandler.handleFile(jdfFile, StandardWatchEventKinds.ENTRY_CREATE);
        FileUtils.cleanDirectory(TestPaths.jdfSuccess.toFile());
    }

    @Test(expected = HandleFileException.class)
    public void testHandleWrongFile() throws Exception {
        Path wrongJdfFile = TestPaths.hotfolder.resolve("wrong.jdf");
        jdfFileHandler.jdfSuccessPath = TestPaths.jdfSuccess;
        jdfFileHandler.jdfFailPath = TestPaths.jdfFailed;
        doThrow(new JdfParsingException("Can't move file.")).when(jdfFileParser).parseJdfFile(eq(wrongJdfFile), Matchers.<Logger>any());
        jdfFileHandler.handleFile(wrongJdfFile, Matchers.<WatchEvent.Kind>any());
        FileUtils.cleanDirectory(TestPaths.jdfFailed.toFile());
    }
}