package de.htwk_leipzig.imn.eduwallchart.server.xslt;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author Georg Felbinger, 23.06.2014
 */
public class XSLTManagerBaseTest {

    private XSLTManagerBase xsltManagerBase;
    private Path xsltPath = Paths.get("src/test/resources/de/htwk_leipzig/imn/eduwallchart/server");

    @Before
    public void setUp() throws Exception {
        xsltManagerBase = new XSLTManagerBase();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testManageXSLT() throws Exception {
        XSLT testXslt = new XSLT("test", xsltPath.resolve("test.xslt"));

        //Hinzufügen eines neuen XSLT
        xsltManagerBase.createOrUpdateXSLT(testXslt);
        assertEquals(testXslt, xsltManagerBase.getXSLTbyName("test"));

        //Hinzufügen des gleichen XSLTs, update erwartet
        xsltManagerBase.createOrUpdateXSLT(testXslt);
        int xsltCount = 0;
        for (XSLT xslt : xsltManagerBase.getXSLTList()) {
            if (xslt != xsltManagerBase.getDefault()) xsltCount++;
        }
        assertEquals(1, xsltCount);

        //Entfernen des XSLTs, bei Abruf nach entfernen Default-XSLT erwartet
        xsltManagerBase.removeXSLT(testXslt.getPath());
        assertEquals(xsltManagerBase.getDefault(), xsltManagerBase.getXSLTbyName("test"));
    }
}