package de.htwk_leipzig.imn.eduwallchart.server.jdf;

import com.google.common.eventbus.EventBus;
import com.google.inject.Provider;
import de.htwk_leipzig.imn.eduwallchart.TestPaths;
import de.htwk_leipzig.imn.eduwallchart.persistence.Job;
import de.htwk_leipzig.imn.eduwallchart.persistence.JobDao;
import de.htwk_leipzig.imn.eduwallchart.persistence.Machine;
import de.htwk_leipzig.imn.eduwallchart.persistence.MachineDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.nio.file.Path;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class JdfHandlingTest {

    JdfFileParser jdfFileParser;
    JdfResourcePoolHandler jdfResourcePoolHandler;
    JdfEntityTransaction jdfEntityTransaction;

    @Mock Logger loggerMock;
    @Mock MachineDao machineDaoMock;
    @Mock JobDao jobDaoMock;
    @Mock EventBus eventBusMock;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        jdfEntityTransaction = new JdfEntityTransaction(jobDaoMock, machineDaoMock, eventBusMock, loggerMock);

        jdfResourcePoolHandler = new JdfResourcePoolHandler();

        jdfFileParser = new JdfFileParser();
        jdfFileParser.jdfEntityManager = new Provider<JdfEntityTransaction>() {
            @Override
            public JdfEntityTransaction get() {
                return jdfEntityTransaction;
            }
        };
        jdfFileParser.jdfResourcePoolHandler = jdfResourcePoolHandler;
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testHandleFile() throws Exception {
    }

    @Test
    public void verifyNumberOfMachines() throws Exception {

        Path jdfFile = TestPaths.home.resolve("test.jdf");
        jdfFileParser.parseJdfFile(jdfFile, loggerMock);
        ArgumentCaptor<Machine> machineArgumentCaptor = ArgumentCaptor.forClass(Machine.class);
        verify(machineDaoMock, times(3)).insertOrUpdateMachine(machineArgumentCaptor.capture());
    }

    @Test
    public void verifyNumberOfJobs() throws Exception {
        Path jdfFile = TestPaths.home.resolve("test.jdf");
        jdfFileParser.parseJdfFile(jdfFile, loggerMock);
        ArgumentCaptor<Job> jobArgumentCaptor = ArgumentCaptor.forClass(Job.class);
        verify(jobDaoMock, times(24)).insertOrUpdateJob(jobArgumentCaptor.capture());
    }

    @Test(expected = JdfParsingException.class)
    public void testWrongJdfFile() throws Exception {
        Path wrongJdfFile = TestPaths.home.resolve("wrong.jdf");

        jdfFileParser.parseJdfFile(wrongJdfFile, loggerMock);
        assertTrue(machineDaoMock.getAllMachines().isEmpty());
    }

    @Test
    public void testInit() throws Exception {

    }
}