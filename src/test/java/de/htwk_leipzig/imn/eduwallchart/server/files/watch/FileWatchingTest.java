package de.htwk_leipzig.imn.eduwallchart.server.files.watch;

import de.htwk_leipzig.imn.eduwallchart.TestPaths;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.FileHandler;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.HandleFileException;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.nio.file.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

public class FileWatchingTest {

    @Mock ExecutorService handlerExecutor, watcherExecutor;
    @Mock FileHandler fileHandler;
    @Mock Logger logger;
    @Mock WatchService watcherService;
    @Mock WatchKey watchKey;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
        FileUtils.cleanDirectory(TestPaths.hotfolder.toFile());
    }

    @Test
    public void testCreateFileWatcher() throws Exception {
        ArrayList<WatchEvent.Kind> eventList = new ArrayList<>();
        eventList.add(StandardWatchEventKinds.ENTRY_CREATE);
        FileWatcher fileWatcher = FileWatcherFactory.createFileWatcher(handlerExecutor, fileHandler, TestPaths.hotfolder, logger, eventList);
        fileWatcher.watcherExecutor = watcherExecutor;

        assertNotNull(fileWatcher);
        assertEquals(fileHandler, fileWatcher.fileHandler);
        assertEquals(TestPaths.hotfolder, fileWatcher.dir);
        assertEquals(eventList, fileWatcher.events);

        ArgumentCaptor<Runnable> runWatcher = ArgumentCaptor.forClass(Runnable.class);
        fileWatcher.start();
        verify(watcherExecutor).execute(runWatcher.capture());
        assertEquals(1, runWatcher.getAllValues().size());

        Runnable runnable = runWatcher.getValue();
        Thread watcherThread = new Thread(runnable);
        watcherThread.start();
        Thread.sleep(500);
        watcherThread.interrupt();

        fileWatcher.stop();
        verify(watcherExecutor).shutdownNow();
    }

    @Test
    public void testAddEvent() throws Exception {
        FileWatcher fileWatcher = FileWatcherFactory.createFileWatcher(handlerExecutor, fileHandler, TestPaths.hotfolder, logger, new ArrayList<WatchEvent.Kind>());

        fileWatcher.addEvent(StandardWatchEventKinds.ENTRY_CREATE);

        assertEquals(fileWatcher.events.size(), 1);
        assertEquals(fileWatcher.events.get(0), StandardWatchEventKinds.ENTRY_CREATE);
    }

    @Test
    public void testSearchDirectory() throws Exception {
        FileWatcher fileWatcher = FileWatcherFactory.createFileWatcher(handlerExecutor, fileHandler, TestPaths.hotfolder, logger, new ArrayList<WatchEvent.Kind>());
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);

        Path testFile = TestPaths.hotfolder.resolve("testfile.txt");
        Files.createFile(testFile);
        fileWatcher.searchDirectory();
        verify(handlerExecutor).submit(runnableArgumentCaptor.capture());

        doThrow(new HandleFileException("testexception"))
                .when(fileHandler).handleFile(testFile.toAbsolutePath(), StandardWatchEventKinds.ENTRY_CREATE);
        for (Runnable runnable : runnableArgumentCaptor.getAllValues()) runnable.run();
        verify(logger).error(anyString());

        Files.delete(testFile);
    }
}