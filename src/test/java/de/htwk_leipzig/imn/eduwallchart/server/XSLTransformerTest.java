package de.htwk_leipzig.imn.eduwallchart.server;

import com.google.common.base.Optional;
import de.htwk_leipzig.imn.eduwallchart.TestPaths;
import de.htwk_leipzig.imn.eduwallchart.persistence.Job;
import de.htwk_leipzig.imn.eduwallchart.persistence.JobStatus;
import de.htwk_leipzig.imn.eduwallchart.persistence.Machine;
import de.htwk_leipzig.imn.eduwallchart.persistence.MachineStatus;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;

import static org.junit.Assert.assertEquals;

public class XSLTransformerTest {

    private XSLTransformer xslTransformer = new XSLTransformer();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetMachineTooltip() throws Exception {
        Machine testMachine = new Machine("testId", "testName", MachineStatus.COMPLETE);
        Path testXslt = TestPaths.home.resolve("transformerTest.xslt");
        String expected = "<div class=\"property\">\n" +
                "<div class=\"name\">ID:</div>\n" +
                "<div class=\"value\">testId</div>\n" +
                "</div><div class=\"property\">\n" +
                "<div class=\"name\">Name:</div>\n" +
                "<div class=\"value\">testName</div>\n" +
                "</div><div class=\"property\">\n" +
                "<div class=\"name\">Status:</div>\n" +
                "<div class=\"value\">COMPLETE</div>\n" +
                "</div>\n";

        assertEquals(expected, xslTransformer.getMachineTooltip(testMachine, testXslt));

    }

    @Test
    public void testGetJobTooltip() throws Exception {
        Job testJob = new Job("testId", "testName", JobStatus.TESTRUNINPROGRESS
                , Optional.<DateTime>absent(), Optional.<DateTime>absent());
        Path testXslt = TestPaths.home.resolve("transformerTest.xslt");
        String expected = "<div class=\"property\">\n" +
                "<div class=\"name\">ID:</div>\n" +
                "<div class=\"value\">testId</div>\n" +
                "</div><div class=\"property\">\n" +
                "<div class=\"name\">Name:</div>\n" +
                "<div class=\"value\">testName</div>\n" +
                "</div>\n";

        assertEquals(expected, xslTransformer.getJobTooltip(testJob, testXslt));
    }
}