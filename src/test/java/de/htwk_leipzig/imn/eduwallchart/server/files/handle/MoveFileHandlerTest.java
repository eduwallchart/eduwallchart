package de.htwk_leipzig.imn.eduwallchart.server.files.handle;

import com.google.common.base.Optional;
import de.htwk_leipzig.imn.eduwallchart.TestPaths;
import de.htwk_leipzig.imn.eduwallchart.server.jdf.JdfFileHandler;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.util.concurrent.ExecutorService;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.mockito.Matchers.eq;

/**
 * @author Peter Nancke
 */
public class MoveFileHandlerTest {
    @Mock Path path;
    @Mock ExecutorService sequentialExecuter;
    @Mock JdfFileHandler jdfFileHandler;
    @Mock Logger logger;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        deleteAllFilesInPath(TestPaths.hotfolder);
        deleteAllFilesInPath(TestPaths.jdfSuccess);
        deleteAllFilesInPath(TestPaths.jdfFailed);
    }

    @After
    public void tearDown() throws Exception {
        deleteAllFilesInPath(TestPaths.hotfolder);
        deleteAllFilesInPath(TestPaths.jdfSuccess);
        deleteAllFilesInPath(TestPaths.jdfFailed);
    }

    @Test
    public void testHandleFile() throws Exception {
        int numberOfFilesInDir = 0;

        Path jdfOrigin = TestPaths.home.resolve("test.jdf");
        final Path jdfFile = TestPaths.hotfolder.resolve("test.jdf");
        File f = new File(String.valueOf(jdfFile));
        if (!f.exists() || f.isDirectory()) {
            try {
                Files.copy(jdfOrigin, jdfFile, REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        MoveFileHandler fileHandler = new MoveFileHandler(logger, TestPaths.jdfSuccess, TestPaths.jdfFailed, jdfFileHandler);

        fileHandler.handleFile(jdfFile, StandardWatchEventKinds.ENTRY_CREATE);

        Thread.sleep(2000);

        Optional<File[]> fileList = Optional.fromNullable(TestPaths.jdfSuccess.toFile().listFiles());
        numberOfFilesInDir = (fileList.or(new File[0])).length;

        TestCase.assertEquals(1, numberOfFilesInDir);
    }

    @Test(expected = HandleFileException.class)
    public void testHandleFalseFile() throws Exception {
        int numberOfFilesInDir = 0;

        Path jdfOrigin = TestPaths.home.resolve("wrong.jdf");
        final Path jdfFile = TestPaths.hotfolder.resolve("wrong.jdf");
        File f = new File(String.valueOf(jdfFile));
        if (!f.exists() || f.isDirectory()) {
            try {
                Files.copy(jdfOrigin, jdfFile, REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        MoveFileHandler fileHandler = new MoveFileHandler(logger, TestPaths.jdfSuccess, TestPaths.jdfFailed, jdfFileHandler);

        Mockito.doThrow(new HandleFileException(TestPaths.jdfFailed.toString())).when(jdfFileHandler).handleFile(eq(jdfFile), Matchers.<WatchEvent.Kind>any());
        fileHandler.handleFile(jdfFile, StandardWatchEventKinds.ENTRY_CREATE);


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Optional<File[]> fileList = Optional.fromNullable(TestPaths.jdfFailed.toFile().listFiles());
        numberOfFilesInDir = (fileList.or(new File[0])).length;

        TestCase.assertEquals(1, numberOfFilesInDir);
    }

    private void deleteAllFilesInPath(Path deletePath) {
        File f = new File(deletePath.toString());
        Optional<File[]> fileList = Optional.fromNullable(f.listFiles());
        if (fileList.isPresent()) {
            for (File file : fileList.get()) {
                file.delete();
            }
        }
    }
}