package de.htwk_leipzig.imn.eduwallchart.server.xslt;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.nio.file.Path;

import static org.junit.Assert.assertEquals;

/**
 * @author Georg Felbinger, 23.06.2014
 */
public class XSLTTest {

    @Mock Path pathMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testXSLT() throws Exception {
        XSLT xslt = new XSLT("test", pathMock);
        assertEquals(xslt.getName(), "test");
        assertEquals(xslt.getPath(), pathMock);
        assertEquals(xslt.toString(), "test");
    }

}