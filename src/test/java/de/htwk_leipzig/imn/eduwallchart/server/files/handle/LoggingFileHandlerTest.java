package de.htwk_leipzig.imn.eduwallchart.server.files.handle;

import com.google.common.base.Optional;
import de.htwk_leipzig.imn.eduwallchart.TestPaths;
import de.htwk_leipzig.imn.eduwallchart.server.jdf.JdfFileHandler;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.concurrent.ExecutorService;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.mockito.Matchers.eq;

/**
 * @author Peter Nancke
 */
public class LoggingFileHandlerTest {
    @Mock Path path;
    @Mock ExecutorService sequentialExecuter;
    @Mock JdfFileHandler jdfFileHandler;
    @Mock Logger logger;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        deleteAllFilesInPath(TestPaths.logs);
        deleteAllFilesInPath(TestPaths.hotfolder);
    }

    @After
    public void tearDown() throws Exception {
        deleteAllFilesInPath(TestPaths.logs);
        deleteAllFilesInPath(TestPaths.hotfolder);
    }

    @Test(expected = HandleFileException.class)
    public void testHandleFile() throws Exception {
        int numberOfFilesInDir = 0;

        Path jdfOrigin = TestPaths.home.resolve("wrong.jdf");
        final Path jdfFile = TestPaths.hotfolder.resolve("wrong.jdf");

        File f = new File(String.valueOf(jdfFile));
        if (!f.exists() || f.isDirectory()) {
            try {
                Files.copy(jdfOrigin, jdfFile, REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        LoggingFileHandler fileHandler = new LoggingFileHandler(logger, TestPaths.logs, jdfFileHandler);
        Mockito.doThrow(new HandleFileException(TestPaths.logs.toString()))
                .when(jdfFileHandler).handleFile(eq(jdfFile), Matchers.<WatchEvent.Kind>any());
        fileHandler.handleFile(jdfFile, Matchers.<WatchEvent.Kind>any());
        Thread.sleep(2000);

        File tmp = new File(String.valueOf(TestPaths.logs.toAbsolutePath()));

        for (File file : tmp.listFiles()) {
            numberOfFilesInDir++;
        }
        TestCase.assertEquals(1, numberOfFilesInDir);
    }

    private void deleteAllFilesInPath(Path deletePath) {
        File f = new File(deletePath.toString());
        Optional<File[]> fileList = Optional.fromNullable(f.listFiles());
        if (fileList.isPresent()) {
            for (File file : fileList.get()) {
                file.delete();
            }
        }
    }
}