package de.htwk_leipzig.imn.eduwallchart.server.xslt;

import de.htwk_leipzig.imn.eduwallchart.TestPaths;
import de.htwk_leipzig.imn.eduwallchart.server.files.handle.HandleFileException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;

/**
 * @author Georg Felbinger, 23.06.2014
 */
public class XSLTFileHandlerTest {

    @Mock XSLTManager xsltManagerMock;
    @Mock Logger loggerMock;
    private XSLTFileHandler xsltFileHandler;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        xsltFileHandler = new XSLTFileHandler(loggerMock, xsltManagerMock);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testDeleteFile() throws Exception {
        Path testXslt = TestPaths.home.resolve("test.xslt");
        xsltFileHandler.handleFile(testXslt, StandardWatchEventKinds.ENTRY_DELETE);
        Mockito.verify(xsltManagerMock).removeXSLT(testXslt);
    }

    @Test(expected = HandleFileException.class)
    public void testWrongFile() throws Exception {
        Path testXslt = TestPaths.home.resolve("wrong.xslt");
        xsltFileHandler.handleFile(testXslt, StandardWatchEventKinds.ENTRY_CREATE);
    }

    @Test
    public void testXSLTFile() throws Exception {
        Path testXslt = TestPaths.home.resolve("test.xslt");
        xsltFileHandler.handleFile(testXslt, StandardWatchEventKinds.ENTRY_CREATE);
        Mockito.verify(xsltManagerMock).createOrUpdateXSLT(Mockito.any(XSLT.class));
    }
}