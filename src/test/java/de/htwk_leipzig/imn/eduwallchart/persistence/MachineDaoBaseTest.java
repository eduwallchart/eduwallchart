package de.htwk_leipzig.imn.eduwallchart.persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MachineDaoBaseTest {

    MachineDaoBase machineDao = new MachineDaoBase();
    @Mock EntityManager entityManagerMock;
    @Mock EntityTransaction entityTransactionMock;
    @Mock Query queryMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        machineDao.manager = entityManagerMock;
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testInsertOrUpdateMachine() throws Exception {
        Machine machineIn = new Machine();
        when(entityManagerMock.getTransaction()).thenReturn(entityTransactionMock);
        Machine machineOut = machineDao.insertOrUpdateMachine(machineIn);
        assertEquals(machineIn.toString(), machineOut.toString());
    }

    @Test
    public void testCreateOrUpdateMachine() throws Exception {
        Machine machineIn = new Machine();
        machineIn.setId("1");
        machineIn.setName("Maschinenname");
        machineIn.setMachineStatus(MachineStatus.AVAILABLE);
        ArgumentCaptor<Machine> machineArgumentCaptor = ArgumentCaptor.forClass(Machine.class);


        when(entityManagerMock.createQuery("SELECT m FROM Machine m WHERE m.id = :id")).thenReturn(queryMock);
        when(queryMock.setParameter("id", machineIn.getId())).thenReturn(queryMock);
        when(entityManagerMock.getTransaction()).thenReturn(entityTransactionMock);

        //Einfügen einer noch nicht existierenden Maschine, diese muss mit persist() weitergegeben werden
        machineDao.createOrUpdateMachine(machineIn.getId(), machineIn.getName(), machineIn.getMachineStatus());
        verify(entityTransactionMock).begin();
        verify(entityManagerMock, times(1)).persist(machineArgumentCaptor.capture());

        //Einfügen einer bereits existenten Maschine, diese muss mit merge() weitergegeben werden
        List<Machine> machineList = new ArrayList<Machine>();
        machineList.add(machineIn);
        when(queryMock.getResultList()).thenReturn(machineList);
        machineDao.createOrUpdateMachine(machineIn.getId(), machineIn.getName(), machineIn.getMachineStatus());
        verify(entityManagerMock, times(1)).merge(machineArgumentCaptor.capture());

        when(entityManagerMock.createQuery("SELECT m FROM Machine m")).thenReturn(queryMock);
        when(queryMock.getResultList()).thenReturn(machineList);
        assertEquals(machineDao.getAllMachines().size(), 1);
    }
}