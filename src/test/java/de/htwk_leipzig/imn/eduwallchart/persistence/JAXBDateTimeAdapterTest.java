package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Optional;
import junit.framework.TestCase;
import org.joda.time.DateTime;

public class JAXBDateTimeAdapterTest extends TestCase {

    JAXBDateTimeAdapter adapter = new JAXBDateTimeAdapter();
    String stringTime = "25.06.2014, 13:37";
    DateTime dateTime = new DateTime(2014, 6, 25, 13, 37, 0);

    public void testUnmarshal() throws Exception {
        Optional<DateTime> methodValue = adapter.unmarshal(stringTime);
        if (methodValue.isPresent())
            assertEquals(methodValue.get(), dateTime);
    }

    public void testMarshal() throws Exception {
        String methodValue = adapter.marshal(Optional.fromNullable(dateTime));
        assertEquals(methodValue, stringTime);
    }
}
