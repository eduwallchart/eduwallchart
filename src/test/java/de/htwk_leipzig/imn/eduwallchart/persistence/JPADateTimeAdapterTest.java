package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Optional;
import junit.framework.TestCase;
import org.joda.time.DateTime;

public class JPADateTimeAdapterTest extends TestCase {

    JPADateTimeAdapter adapter = new JPADateTimeAdapter();
    DateTime dateTime = new DateTime(2014, 6, 25, 13, 37, 0);
    String stringTime = dateTime.toString();

    public void testConvertToDatabaseColumn() throws Exception {
        String methodValue = adapter.convertToDatabaseColumn(Optional.fromNullable(dateTime));
        assertEquals(methodValue, dateTime.toString());
    }

    public void testConvertToEntityAttribute() throws Exception {
        Optional<DateTime> methodValue = adapter.convertToEntityAttribute(stringTime);
        if (methodValue.isPresent())
            assertEquals(methodValue.get().toString(), dateTime.toString());
    }
}
