package de.htwk_leipzig.imn.eduwallchart.persistence;

import com.google.common.base.Optional;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class JobDaoBaseTest {

    JobDaoBase jobDao = new JobDaoBase();
    @Mock EntityManager entityManagerMock;
    @Mock EntityTransaction entityTransactionMock;
    @Mock Query queryMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        jobDao.manager = entityManagerMock;
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testInsertOrUpdateJob() throws Exception {
        Job jobIn = new Job();
        when(entityManagerMock.getTransaction()).thenReturn(entityTransactionMock);
        Job jobOut = jobDao.insertOrUpdateJob(jobIn);
        assertEquals(jobIn, jobOut);
    }

    @Test
    public void testCreateOrUpdateJob() throws Exception {
        Optional<DateTime> start = Optional.fromNullable(DateTime.now());
        Optional<DateTime> end = Optional.fromNullable(DateTime.now());

        Job jobIn = new Job("1", "Name", JobStatus.INPROGRESS, start, end);

        when(entityManagerMock.createQuery("SELECT j FROM Job j WHERE j.id = :id")).thenReturn(queryMock);
        when(queryMock.setParameter("id", jobIn.getId())).thenReturn(queryMock);
        when(entityManagerMock.getTransaction()).thenReturn(entityTransactionMock);

        Job jobOut = jobDao.createOrUpdateJob(jobIn.getId(), jobIn.getName(), jobIn.getStatus(), jobIn.getStartTime(), jobIn.getEndTime());
        assertEquals(jobIn.getId(), jobOut.getId());

        jobIn.setName("NeuerName");
        ArrayList<Job> jobList = new ArrayList<>();
        jobList.add(jobIn);

        when(queryMock.getResultList()).thenReturn(jobList);
        jobOut = jobDao.createOrUpdateJob(jobIn.getId(), jobIn.getName(), jobIn.getStatus(), jobIn.getStartTime(), jobIn.getEndTime());
        assertEquals(jobIn.getId() + jobIn.getName(), jobOut.getId() + jobOut.getName());
    }

    @Test
    public void testCreateJob() throws Exception {
        // Bereits mit testCreateOrUpdateJob() abgedeckt
    }

    @Test
    public void testUpdateJob() throws Exception {
        Optional<DateTime> start = Optional.fromNullable(DateTime.now());
        Optional<DateTime> end = Optional.fromNullable(DateTime.now());

        Job jobIn = new Job("1", "Name", JobStatus.INPROGRESS, start, end);
        when(entityManagerMock.getTransaction()).thenReturn(entityTransactionMock);

        Job jobOut = jobDao.insertOrUpdateJob(jobIn);

        assertEquals(jobIn, jobOut);
    }

    @Test
    public void testGetJob() throws Exception {
        Optional<DateTime> start = Optional.fromNullable(DateTime.now());
        Optional<DateTime> end = Optional.fromNullable(DateTime.now());

        Job jobIn = new Job("1", "Name", JobStatus.INPROGRESS, start, end);
        ArrayList<Job> jobList = new ArrayList<>();
        jobList.add(jobIn);

        when(entityManagerMock.createQuery("SELECT j FROM Job j WHERE j.id = :id")).thenReturn(queryMock);
        when(queryMock.setParameter("id", jobIn.getId())).thenReturn(queryMock);
        when(queryMock.getResultList()).thenReturn(jobList);

        Optional<Job> jobOutTest = jobDao.getJob(jobIn.getId());

        assertEquals(jobOutTest.get(), jobIn);

        when(queryMock.getResultList()).thenReturn(new ArrayList<Job>());

        Optional<Job> jobOutWrong = jobDao.getJob(jobIn.getId());

        assertEquals(Optional.<Job>absent(), jobOutWrong);
    }

    @Test
    public void testGetAllJobs() throws Exception {
        Optional<DateTime> start = Optional.fromNullable(DateTime.now());
        Optional<DateTime> end = Optional.fromNullable(DateTime.now());

        Job jobIn = new Job("1", "Name", JobStatus.INPROGRESS, start, end);
        ArrayList<Job> jobListIn = new ArrayList<>();
        jobListIn.add(jobIn);

        when(entityManagerMock.createQuery("SELECT j FROM Job j ORDER BY j.startTime")).thenReturn(queryMock);
        when(queryMock.getResultList()).thenReturn(jobListIn);

        ArrayList<Job> jobListOut = (ArrayList<Job>) jobDao.getAllJobs();

        assertTrue(jobListIn.equals(jobListOut));
    }


}