package de.htwk_leipzig.imn.eduwallchart;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Die Klasse stellt Pfade für Tests bereit, die mit Dateien arbeiten
 *
 * @author Georg Felbinger
 */
public class TestPaths {
    public static final Path home = Paths.get("src/test/resources/testHomeDir");
    public static final Path hotfolder = home.resolve("hotfolder");
    public static final Path impressum = home.resolve("impressum");
    public static final Path jdf = home.resolve("jdf");
    public static final Path jdfFailed = jdf.resolve("failed");
    public static final Path jdfSuccess = jdf.resolve("success");
    public static final Path logs = home.resolve("logs");
    public static final Path templates = home.resolve("templates");
    public static final Path templatesJobs = templates.resolve("jobs");
    public static final Path templatesMachines = templates.resolve("machines");
}
